package exerciciZooJava;

import java.util.Scanner;

public class PrincipalZoo {

	public static void main(String[] args) {
		Espectador persona = new Espectador();
		Zoo zoo = new Zoo(10, persona);
		String comandos = new String();
		
		Scanner scan = new Scanner (System.in);
		System.out.println("El zoo obre les seves portes.");
		
		do{
			System.out.println("Escriu un comandament:");
			comandos = scan.nextLine();
			comandos = comandos.toLowerCase();
			
			if (comandos.equals("afegeix vaca")){
				zoo.afegeixAnimal("vaca");
				System.out.println("Ha arribat al zoo una enorme vaca");
			}
			else if (comandos.equals("afegeix cocodril")){
				zoo.afegeixAnimal("cocodril");
				System.out.println("Ha arribat al zoo un perill�s cocodril");
			}
			
			else if (comandos.equals("suprimeix vaca")){
				zoo.suprimeixAnimal("vaca");
				System.out.println("S'ha traslladat una vaca a un altre zoo");
			}
			else if (comandos.equals("suprimeix cocodril")){
				zoo.suprimeixAnimal("cocodril");
				System.out.println("S'ha traslladat un cocodril a un altre zoo");
			}
			else if(comandos.equals("suprimeix tots vaca")){
				zoo.suprimeixTots("vaca");
				System.out.println("El zoo deixa de tenir vaques");
			}
			else if(comandos.equals("suprimeix tots cocodril")){
				zoo.suprimeixTots("cocodril");
				System.out.println("El zoo deixa de tenir cocodrils");
			}
			else if(comandos.equals("mira")){
				System.out.println(zoo.mira());
			}
			else{
				System.out.println("Comanda incorrecta");
			}
		}while(comandos!="surt");
		
				
	}

}
