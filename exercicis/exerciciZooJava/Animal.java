package exerciciZooJava;

import java.util.Random;

public abstract class Animal implements Esser{
	
	public abstract String mou(Zoo zoo);
	public abstract String alimenta(Zoo zoo);
	public abstract String expressa(Zoo zoo);
	
	@Override
	public String accio(Zoo zoo){
		Random rand = new Random();
		int num = rand.nextInt(3);
		String accio = new String();
		
			if (num==0){
				accio = mou(zoo);
			}
			if (num==1){
				accio =  alimenta(zoo);
			}
			if (num==2){
				accio =  expressa(zoo);
			}
			
		return accio;
	}
}
