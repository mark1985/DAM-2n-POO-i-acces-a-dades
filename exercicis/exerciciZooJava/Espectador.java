package exerciciZooJava;

public class Espectador implements Esser{

	@Override
	public String accio(Zoo zoo) {
		String mensaje=new String();
		Animal espectador = zoo.mostraAnimal();
		
		if (espectador instanceof Vaca){
			mensaje="Un espectador mira una vaca";
		}
		else if (espectador instanceof Cocodril){
			mensaje="Un espectador mira a un perillos cocodril";
		}
		else if (espectador == null){
			mensaje="Un espectador no sap a on mirar perqu� no troba animals";
		}
		return mensaje;
	}

}
