package exerciciZooJava;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Zoo {
	private List<Animal> animals;
	private Espectador espectador;
	
	public Zoo (int num, Espectador espectador){
		this.animals = new ArrayList<Animal>(num);
		this.espectador = espectador;
	}
	
	public Animal mostraAnimal(){
		Random rand = new Random();
		int num;
		
		if(this.animals.size()==0){
			return null;
		}
		else{
			num = rand.nextInt(this.animals.size());
		}
		
		if(this.animals.get(num)!=null){
			return (Animal) this.animals.get(num);
		}
		else{
			return null;
		}
	}
	
	public void afegeixAnimal(String nomAnimal){
		if (nomAnimal.equals("vaca")){
			this.animals.add(new Vaca());
		}
		if (nomAnimal.equals("cocodril")){
			this.animals.add(new Cocodril());
		}
	}
	
	public void suprimeixAnimal(String nomAnimal){
		int i=0;
		boolean control = false;
		
		
		while (control == false && i < this.animals.size()){
						
			if (nomAnimal.equals("vaca")){
				if (this.animals.get(i) instanceof Vaca){
					this.animals.remove(i);
					control = true;
				}
			}
			if (nomAnimal.equals("cocodril")){
				if (this.animals.get(i) instanceof Cocodril){
					this.animals.remove(i);
					control = true;
				}
			}
			i++;
		}
		
	}
	
	public void suprimeixAnimal(Animal objAnimal){
		int i=0;
		
		this.animals.remove(objAnimal);
		
	}
	
	public void suprimeixTots (String nomAnimal){
		
		for (Animal i : this.animals){
			
			if (nomAnimal.equals("vaca") && i instanceof Vaca){
				this.animals.remove(i);
			}
			if (nomAnimal.equals("cocodril") && i instanceof Cocodril){
				this.animals.remove(i);
			}

		}
	}
	
	public String mira(){
		Random rand = new Random();
		Animal animalMira;
		String res = new String();
		
		int num = rand.nextInt(2);

		if (num == 0){
			res = this.espectador.accio(this);
		}
		if (num == 1){
			animalMira = mostraAnimal();
			if(animalMira!=null){
				res = animalMira.accio(this);
			}
			else{
				res = ("Quin zoo mes avorrit! No te cap animal");
			}
		}
		return res;
	}
}
