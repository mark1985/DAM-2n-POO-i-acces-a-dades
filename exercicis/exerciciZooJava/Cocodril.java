package exerciciZooJava;

public class Cocodril extends Animal{

	@Override
	public String mou(Zoo zoo) {
		return  "Un cocodril neda estany amunt estany avall";
	}

	@Override
	public String alimenta(Zoo zoo) {
		String mensaje=new String();
		Animal alimentaCocodril = zoo.mostraAnimal();
		
			if(alimentaCocodril!=null && alimentaCocodril != this){
				if(alimentaCocodril instanceof Vaca){
					mensaje = "Un cocodril es menja una vaca!";
				}
				if(alimentaCocodril instanceof Cocodril){
					mensaje = "Un cocodril es menja un altre cocodril";
				}
				zoo.suprimeixAnimal(alimentaCocodril);
			}
			else{
				mensaje = "Un cocodril busca a qui es pot menjar";
			}
		return mensaje;
	}

	@Override
	public String expressa(Zoo zoo) {
		return "Un cocodril obre una boca plena de dents";
	}

}
