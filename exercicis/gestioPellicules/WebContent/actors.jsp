<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="beans.Actor,java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Llista d'actors i pel·lícules</title>
</head>
<body>
<h1>Llista d'actors i pel·lícules</h1>
<table border='1'>
<tr><th>Codi</th><th>Nom</th><th>Cognom</th><th>Pel·lícules</th></tr>
<%
@SuppressWarnings("unchecked")
List<Actor> actors = (List<Actor>) request.getAttribute("actors");
for (Actor a : actors) {%>
	<tr><td><%=a.getActorId() %></td>
	<td><%=a.getFirstName() %></td>
	<td><%=a.getLastName() %></td>
	<td><%=a.getFilms() %></td></tr>
<%}%>
</table>
<a href="index.html">Inici</a>
</body>
</html>