package beans;

public class Language {
	//private Integer languageId;
	private String languageName;
	
	public Language(Integer id, String name){
		//this.languageId = id;
		this.languageName = name;
	}
	public Language() {
	}
	
	public String get(){
		return languageName;
	}
}
