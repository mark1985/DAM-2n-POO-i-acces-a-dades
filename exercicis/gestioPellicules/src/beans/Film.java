package beans;

public class Film {
	private int filmId;
	private String title;
	private String description;
	private String releaseYear;
	private Integer languageId;
	private Integer originalLanguageId;
	private int length;
	
	public int getFilmId() {
		return filmId;
	}
	
	public Film(String title, String year){
		this.title = title;
		this.releaseYear = year;
	}
	
	public Film(int filmId, String title, String description,
			String releaseYear, Integer languageId, Integer originalLanguageId,
			int length) {

		this.filmId = filmId;
		this.title = title;
		this.description = description;
		this.releaseYear = releaseYear;
		this.languageId = languageId;
		this.originalLanguageId = originalLanguageId;
		this.length = length;
	}
	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	public Integer getLanguageId() {
		return languageId;
	}
	/*public String getLanguageName() {
		//TODO METODO PARA OBTENER SOLO EL NOMBRE DEL LENGUAJE BIEN HECHO return Language.get();
	}*/
	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}
	public Integer getOriginalLanguageId() {
		return originalLanguageId;
	}
	public void setOriginalLanguageId(Integer originalLanguageId) {
		this.originalLanguageId = originalLanguageId;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}
