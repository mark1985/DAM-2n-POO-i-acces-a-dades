package beans;

import java.util.List;

public class Actor {
	private int actorId;
	private String firstName;
	private String lastName;
	private List<Film> filmList;
	
	public Actor(int id, String name, String lastName){
		this.actorId = id;
		this.firstName = name;
		this.lastName = lastName;
	}
	
	public int getActorId() {
		return actorId;
	}
	public void setActorId(int actorId) {
		this.actorId = actorId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFilms() {
		String films="";
		
		for(int i=0; i<filmList.size(); i++){
			films+=filmList.get(i).getTitle()+" ("+filmList.get(i).getReleaseYear().substring(0, 4)+"); ";
		}
		
		return films;
	}
	public void setFilmList(List<Film> filmList) {
		this.filmList = filmList;
	}
}
