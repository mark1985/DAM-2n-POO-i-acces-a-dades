package accessBD;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import beans.Actor;
import beans.Film;
import beans.Language;

public class OperacionsBD {
	
	protected Connection getConnection() throws SQLException, ClassNotFoundException {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/sakila",
						"root","");

		return conn;
	}
	
	public List<Film> consultaPellicules() throws SQLException, ClassNotFoundException {
		String sql = "select * from film;";
		List<Film> pellicules = new ArrayList<Film>();
		Class.forName("com.mysql.jdbc.Driver");
		
			try (Connection conn = getConnection(); PreparedStatement st = conn.prepareStatement(sql)) {
				
				try (ResultSet rs = st.executeQuery(sql)){
					while (rs.next()) {
						Film f = new Film(rs.getInt("film_id"), rs.getString("title"),
								rs.getString("description"), rs.getString("release_year"),
								rs.getInt("language_id"), rs.getInt("original_language_id"),
								rs.getInt("length"));
						pellicules.add(f);
					}
				}
			}catch (SQLException e) {
			    System.err.println("Error SQL: "+e.getMessage());
			}

		return pellicules;
	}
	
	public List<Film> consultaPelliculesPerActor(int id) throws SQLException, ClassNotFoundException {
		String sql = "select title, release_year from film "+
				"join film_actor on film.film_id=film_actor.film_id "+
					"where film_actor.actor_id = "+id+
					" order by release_year asc;";
		List<Film> pellicules = new ArrayList<Film>();
		
		try (Connection conn = getConnection(); PreparedStatement st = conn.prepareStatement(sql)) {
			
			try (ResultSet rs = st.executeQuery(sql)){
				while (rs.next()) {
					Film f = new Film(rs.getString("title"), rs.getString("release_year"));
					pellicules.add(f);
				}
			}
		}catch (SQLException e) {
		    System.err.println("Error SQL: "+e.getMessage());
		}
		return pellicules;
	}
	
	public List<Actor> consultaActors() throws SQLException, ClassNotFoundException {
		String sql = "select actor_id, first_name, last_name from actor;";
		List<Actor> actors = new ArrayList<Actor>();
		
		try (Connection conn = getConnection(); PreparedStatement st = conn.prepareStatement(sql)) {
			try (ResultSet rs = st.executeQuery(sql)){
				while (rs.next()) {
					Actor a = new Actor(rs.getInt("actor_id"), rs.getString("first_name"), rs.getString("last_name"));
					actors.add(a);
				}
			}
			
		}catch (SQLException e) {
		    System.err.println("Error SQL: "+e.getMessage());
		}
		
		for(Actor a : actors){
			a.setFilmList(consultaPelliculesPerActor(a.getActorId()));
		}
		
		return actors;
	}
	
	public Language consultaLanguage(Integer id){
		String sql = "select language_id, name from language where language_id = ?;";
		Language lang = new Language();
		
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); PreparedStatement st = conn.prepareStatement(sql)) {

		        st.setInt(1, id);
		        try (ResultSet rs = st.executeQuery()) {
		            while (rs.next()) {
		            	lang = new Language(rs.getInt("language_id"), rs.getString("name"));
	                }
	            }
		        
		}catch (SQLException e) {
		    System.err.println("Error SQL: "+e.getMessage());
		}
		
		return lang;
	}
}
