package fitxers8;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GrepComplex {

	public static void main(String[] args) {
		
		String str;
		
		if(args.length>=2){
			Path rutaFitxer = Paths.get(args[1]);
			
			if(Files.isDirectory(rutaFitxer)){
				try (DirectoryStream<Path> stream = Files.newDirectoryStream(rutaFitxer)) {
					for (Path file: stream) {
						if (Files.isRegularFile(file)){
							try (LineNumberReader lector = new LineNumberReader(new FileReader(file.toString()))) {
				    			while ((str = lector.readLine()) != null) {
				    				if (str.contains(args[0])){
				    					System.out.println(str);
				    				}	
				    			}
				    		} catch (IOException e) {
				    			System.err.println("Error de lectura: " + e.getMessage());
				    			e.printStackTrace();
				    		}
						}
					}
				} catch (IOException | DirectoryIteratorException ex) {		    
					System.err.println(ex);
				}
			}
			else if(Files.isRegularFile(rutaFitxer)){
				try (LineNumberReader lector = new LineNumberReader(new FileReader(args[1]))) {
	    			while ((str = lector.readLine()) != null) {
	    				if (str.contains(args[0])){
	    					System.out.println((lector.getLineNumber() + 1) + ":" + str);
	    				}	
	    			}
	    		} catch (IOException e) {
	    			System.err.println("Error de lectura: " + e.getMessage());
	    			e.printStackTrace();
	    		}
			}
		}
		else{
			System.err.println("Cagada pastoret");
		}
	}

}
