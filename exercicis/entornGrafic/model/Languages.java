package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class Languages {
	
	private static Languages languages;
	private Map<Integer, String> idioma = new HashMap<Integer, String>();
	
	public static Languages getInstance(){
		if(languages == null){
			languages = new Languages();
		}
		return languages;
	}
	
	public String getLanguage(int langId){
		return idioma.get(langId);
	}
	
	public void init(){
		String sql = "select language_id, name from language";

		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?user=root");
				Statement st = con.createStatement()){
					
				try(ResultSet rs = st.executeQuery(sql)){
					
					while(rs.next()){
						this.idioma.put(rs.getInt("language_id"), rs.getString("name"));
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}
	
	private Languages(){
		init();
	}
}
