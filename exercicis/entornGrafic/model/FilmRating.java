package model;

public class FilmRating {
	
	private static String[] ratingEnum = {"G", "PG", "PG13", "R", "NC17"};

	public static String[] getRatingEnum() {
		return ratingEnum;
	}
}