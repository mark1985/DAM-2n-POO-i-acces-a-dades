package insertsUpdates;

import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Update1 {

	public static void main(String[] args) {

		String cp = new String();
		String pop = new String();
		
		Scanner sc = new Scanner(System.in);
		
		MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("zips");
        MongoCollection<Document> collection = db.getCollection("zips");
        
        System.out.println("Escriu el CP de la ciutat que vols");
        cp=sc.nextLine();
        
        Bson filter = Filters.eq("_id",cp);
        
        collection.find(filter).forEach((Document doc) -> System.out.println(doc.toJson()));
        
        System.out.println("Introdueix ara la nova poblacio que vols que tingui la ciutat");
        pop = sc.nextLine();
        
        collection.updateOne(Filters.eq("_id", cp), new Document("$set", new Document("pop", pop)));
        System.out.println(collection.find(Filters.eq("_id", cp)).first().toJson());
        
        
        sc.close();
        client.close(); 

	}

}
