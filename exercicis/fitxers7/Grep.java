package fitxers7;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;

public class Grep {

	public static void main(String[] args) {
        
        String s;
        Boolean grep;

        if (args.length == 2) {
        	try (LineNumberReader lector = new LineNumberReader(new FileReader(args[1]))) {
    			while ((s = lector.readLine()) != null) {
    				if (s.contains(args[0])){
    					System.out.println(s);
    				}	
    			}
    		} catch (IOException e) {
    			System.err.println("Error de lectura: " + e.getMessage());
    			e.printStackTrace();
    		}
        } else {
            System.err.println("Cagada pastoret!");
        }
        
	}

}
