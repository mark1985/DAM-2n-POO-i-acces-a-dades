package fitxers12;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Recompte extends DefaultHandler{
private int tab = 0;
	
	private void tabula() {
		for (int i=0; i<tab; i++)
			System.out.print("  ");
	}
	
	@Override
	public void startDocument() {
		tabula();
		System.out.println("Inici del XML");
		tab++;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		tabula();
		System.out.println("Start element: "+localName);
		tab++;
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		tab--;
		tabula();
		System.out.println("End element: "+localName);
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String contingut = new String(ch, start, length);
		contingut = contingut.replaceAll("[\t\n]", "").trim();
		if (!contingut.equals("")) {
			tabula();
			System.out.println("Caracters: "+contingut);
		}
	}
	
	@Override
	public void endDocument() {
		tab--;
		tabula();
		System.out.println("Final del XML");
	}
}
