
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProvaConnexio {
	
	public static void main(String[] args) {
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root")){
					System.out.println("Connexio exitosa a la base de dades");
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}
}
