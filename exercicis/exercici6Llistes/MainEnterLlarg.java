package exercici6Llistes;

public class MainEnterLlarg {

	public static void main(String[] args) {
		byte[] numByt = {1,4,3,5,2,4,9,8,4,5,6,8,7,9};
		long numLon = 1213142536;
		EnterLlarg num1;
		EnterLlarg num2;
						
		num1 = new EnterLlarg(numByt);
		num2 = new EnterLlarg(numLon);
		
		System.out.println(num1.toString());
		System.out.println(num2.toString());
		
		System.out.println((num1.suma(num2)).toString());
		
	}

}
