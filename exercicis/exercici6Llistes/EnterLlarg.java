package exercici6Llistes;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class EnterLlarg {
	private List<Integer> enterllarg;
	
	public EnterLlarg(){

	}
	
	public EnterLlarg(String num){
		
		this.enterllarg = new ArrayList<Integer>(num.length());
		for(int i=0; i<num.length();i++){
			this.enterllarg.add(Integer.parseInt(num.charAt(i)+""));
		}
	}
	
	public EnterLlarg(long num){
		this(String.valueOf(num));
	}
	
	public EnterLlarg(byte[] num){
		this.enterllarg = new ArrayList<Integer>(num.length);
		
		for(int i=0;i<num.length;i++){
			this.enterllarg.add((int)num[i]);
		}
	}
	
	private int comparadorPerTamany(EnterLlarg n1, EnterLlarg n2){
		int comparacio=0;
			
			if (n1.enterllarg.size()>n2.enterllarg.size()){
				comparacio=1;
			}
		
		return comparacio;
	}
	
	public List<Integer> suma(EnterLlarg num){
		List<Integer> enternou = new ArrayList<Integer>();
		
		if(comparadorPerTamany(this,num)==1){
			enternou=ferSuma(this,num);
		}
		else{
			enternou=ferSuma(num,this);
		}
		
		return enternou;
	}
	
	private List<Integer> ferSuma(EnterLlarg enterGran, EnterLlarg enterPetit){
		List<Integer> sumat = new ArrayList<Integer>();
		ListIterator<Integer> gran;
		ListIterator<Integer> petit;
		Integer result = 0;
		Integer residu = 0;
		Integer sumand1 = 0;
		Integer sumand2 = 0;
		boolean control;
				
		control=false;
		while(control==false){
			
			if(enterPetit.enterllarg.size()<enterGran.enterllarg.size()){
				enterPetit.enterllarg.add(0, 0);
			}
			else{
				control=true;
			}
			
		}
		
		gran = enterGran.enterllarg.listIterator(enterGran.enterllarg.size());
		petit = enterPetit.enterllarg.listIterator(enterPetit.enterllarg.size());
		
		while(petit.hasPrevious()){
			
			if(gran.hasPrevious()){
				sumand1=gran.previous();
			}
			
			if(petit.hasPrevious()!=false){
				sumand2=petit.previous();
			}
			else{
				sumand2=0;
			}
			if(result>9){
				residu=result/10;
			}
			else{
				residu=0;
			}
			result=sumand1+sumand2+residu;
			sumat.add(0, result%10);
		}
		
		return sumat;
	}
		
	@Override
	public String toString(){
		String cadena = new String();
		
		for(int i=0;i<this.enterllarg.size();i++){
			cadena+=this.enterllarg.get(i);
		}
		
		return cadena;
	}
}
