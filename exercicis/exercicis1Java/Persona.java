package exercicis1Java;

public class Persona implements Comparable<Persona> {

	private int pes;
	private int edat;
	private int altura;
	
	@Override
	public int compareTo(Persona p) {
		return getAltura() - p.getAltura();
	}

	public int getAltura() {
		return altura;
	}

}
