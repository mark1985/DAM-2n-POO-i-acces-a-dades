package diccionaris2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class DiccionariEx2Main {

	public static void main(String[] args) {
		
		Map<String,String> llistaPrograma = new HashMap<String,String>();
		int opcion = 0;
		String cadena = new String();
		String cadena2 = new String();
		
		llistaPrograma.put("Firefox", "3.5");
		llistaPrograma.put("Premiere", "CS3");
		llistaPrograma.put("After Effects", "CS3");
		llistaPrograma.put("Gimp", "2.0.1");
		llistaPrograma.put("Python", "2.6.2");
		llistaPrograma.put("SnagIt", "7.0.1");
				
		Scanner scan = new Scanner(System.in);
		
		do{
			System.out.println("1. Mostrar llista\n2. Consultar versi�\n3. Afegir un nou programa\n4. Sortir\n\nTria una opcio");
			opcion = scan.nextInt();
			scan.nextLine();
			
			switch(opcion){
			case 1:
				for(String programa : llistaPrograma.keySet()){
					
					System.out.println(programa+", "+llistaPrograma.get(programa));
				}
				break;
			case 2:
				for(String programa : llistaPrograma.keySet()){
					
					System.out.println(llistaPrograma.get(programa));
				}
				break;
			case 3:
				System.out.println("Escriu el nom del programa");
				cadena = scan.nextLine();
				
				if(llistaPrograma.containsKey(cadena)){
					System.err.println("Aquest programa ja es troba a la llista");
					
				}
				else{
					System.out.println("Escriu la nova versio");
					cadena2 = scan.nextLine();
					llistaPrograma.put(cadena, cadena2);
				}
				break;
			default:System.out.println("Opcio incorrecta");
			}
			
		}while(opcion!=4);
		
		scan.close();

	}

}
