package exercicis4Java;

public class PlantaMain {

	public static void main(String[] args) throws InterruptedException {
		Planta altibus = new Altibus();
		Planta declinus = new Declinus();
		Jardi jardi = new Jardi(15);
		jardi.plantaLlavor(altibus, 2);
		jardi.plantaLlavor(declinus, 8);

		while (true){
			jardi.temps();
			System.out.format(" %s ",jardi.toString());
			Thread.sleep(1000);
		}
		
	}

}
