package exercicis4Java;

public class Declinus extends Planta{
	int volCreixer=0;
	boolean haCrescut=false;
	
	public Declinus(){
		
	}
	
	@Override
	public Llavor creix(){
		Llavor semilla = null;
		if(this.volCreixer%2==0 && this.getAltura()<4 && this.haCrescut==false){
			this.setAltura(this.getAltura()+1);
		}
		
		else if (this.getAltura()==4 && this.haCrescut==false){
			Planta planta = new Declinus();
			semilla = new Llavor(planta);
			this.haCrescut=true;
		}
		
		else if(this.haCrescut==true && this.volCreixer%2==0){
			this.setAltura(this.getAltura()-1);
				
				if (this.getAltura()>=3){
					Planta planta = new Declinus();
					semilla = new Llavor(planta);
				}
		}
		if(this.getAltura()==0){
			this.setEsViva(false);
		}
		volCreixer++;		
		return semilla;
	}

	@Override
	public char getChar(int nivell) {
		char dibuix = 0;
		
		if (nivell < this.getAltura()){
			dibuix = ':';
		}
		if (nivell == this.getAltura()){
			dibuix = '*';
		}
		
		return dibuix;
	}
}
