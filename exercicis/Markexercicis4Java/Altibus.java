package exercicis4Java;

public class Altibus extends Planta{
	
	public Altibus(){
		
	}
	
	public Llavor creix(){
		Llavor semilla = null;
		super.creix();
	
		if (this.getAltura()>7 && this.getAltura()<10){
			Planta planta = new Altibus();
			semilla = new Llavor(planta);
		}
		return semilla;
	}
	
	@Override
	public char getChar(int nivell){
		char dibuix = 0;

		if (nivell < this.getAltura()){
			dibuix = '|';
		}
		if (nivell == this.getAltura()){
			dibuix = 'O';
		}

		
		return dibuix;
	}
	
}
