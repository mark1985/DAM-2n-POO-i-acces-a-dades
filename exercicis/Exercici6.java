package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exercici6 {

	public static void main(String[] args) {
		
		String sql = "select count(film_id), cat.name from film_category fc "
				+ "left join category cat on (cat.category_id=fc.category_id) "
				+ "group by fc.category_id;";

		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); Statement st = con.createStatement()){
					System.out.println("Connexio OK");
					System.out.println();
					
				try(ResultSet rs = st.executeQuery(sql)){
					
					while(rs.next()){
						System.out.println(rs.getString("count(film_id)")+" "+rs.getString("cat.name"));
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}

	}

}
