package fitxers10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class MiniShell {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		Path directori = Paths.get(System.getProperty("user.dir"));
		Path cd;
		Path cat;
		String comanda = new String();
		String[] instruccions;
		String reader = new String();
		
		do {
			System.out.println(directori.normalize() + "> ");
			comanda = scan.nextLine();
			instruccions = comanda.split(" ");
			
				if(instruccions.length>0){
					
					if(instruccions[0].equals("ls")){
						try (DirectoryStream<Path> stream = Files.newDirectoryStream(directori)) {
							for (Path fitxer : stream) {
								
								if(Files.isDirectory(fitxer)){
									System.out.println(fitxer.getFileName() +"/");
								}
								else{
									System.out.println(fitxer.getFileName());
								}
								
							}
						} catch (IOException ex) {
							System.err.println(ex);
						}
						System.out.println();
					}
					else if(instruccions[0].equals("cd")){
						
						if(instruccions.length==2){
							cd = directori.resolve(instruccions[1]);
							if (Files.isDirectory(cd))
								directori = cd.toAbsolutePath().normalize();
							else
								System.err.println("No es troba el directori");
						}
						else{
							System.err.println("Falta introduir el directori de desti");
						}
					}
					else if(instruccions[0].equals("cat")){
						
						if(instruccions.length==2){
							cat = directori.resolve(instruccions[1]);
							
							try (BufferedReader buffer = new BufferedReader(new FileReader(cat.toString()))) {
								
								while ((reader = buffer.readLine()) != null){
									System.out.println(reader);
								}	
								System.out.println();
							}catch (IOException e) {
								System.err.println(e.getMessage());
							}
						}
						else{
							System.err.println("Falta introduir el fitxer");
						}
					}
				}
				else{
					System.err.println("No hi ha comanda");
				}
			
		} while (comanda!="surt");
		
		scan.close();

	}

}
