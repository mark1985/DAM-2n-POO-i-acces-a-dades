package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Exercici7 {

	public static void main(String[] args) {
			
		String sql = "select f.title from film f "
		    + "left join film_actor fa on (f.film_id = fa.film_id) "
		    + "left join actor a on (a.actor_id = fa.actor_id) "
		    + "where a.last_name = ?";
					
		Scanner sc = new Scanner(System.in);
		    String lastName=" ";
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
			"user=root"); PreparedStatement st = conn.prepareStatement(sql)) {
		
			while (!lastName.equals("")) {
		    System.out.println("Cognom a cercar (en blanc per sortir): ");
		    lastName = sc.nextLine();
		    	if (!lastName.equals("")) {
			        st.setString(1, lastName);
			        try (ResultSet rs = st.executeQuery()) {
			            while (rs.next()) {
			                String film = rs.getString("title");
			                    System.out.println(film);
		                }
		            }
		        }
		    }
			
		} catch (SQLException e) {
		    System.err.println("Error SQL: "+e.getMessage());
		}
	        sc.close();

	}

}
