package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Exercici8 {

	public static void main(String[] args) {
		String sql = "select film_id, store_id from inventory where inventory_id in "
				+ "(select inventory_id from rental where return_date is null and customer_id = "
				+ "(select customer_id from customer where (first_name)= ? and (last_name) = ?));";
						
			Scanner sc = new Scanner(System.in);
			String lastName=" ";
			
			try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); PreparedStatement st = conn.prepareStatement(sql)) {
			
				while (!lastName.equals("")) {
			    System.out.println("Nom i Cognom a cercar (en blanc per sortir): ");
			    lastName = sc.nextLine();
			    	if (!lastName.equals("")) {
			    		lastName.split(" ");
				        st.setString(1, lastName.split(" ")[0]);
				        st.setString(2, lastName.split(" ")[1]);
				        try (ResultSet rs = st.executeQuery()) {
				            while (rs.next()) {
				                String film = rs.getString("film_id");
				                film += rs.getString("store_id");
				                    System.out.println(film);
			                }
			            }
			        }
			    }
				
			} catch (SQLException e) {
			    System.err.println("Error SQL: "+e.getMessage());
			}
		        sc.close();

	}

}
