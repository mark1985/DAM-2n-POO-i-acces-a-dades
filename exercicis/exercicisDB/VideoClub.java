package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VideoClub {
	private Connection connection = null;
	private PreparedStatement st;
	private Scanner scanner = new Scanner(System.in);
	
	private void connect() throws SQLException {
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?user=root");
	}
	
	private void disconnect() {
        if (st != null) {
            try {
            	st.close();
            } catch (SQLException e) {
                ;
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                ;
            }
        }
    }
	
	public List<String> getStores() throws SQLException {
        List<String> stores = new ArrayList<String>();
        st = connection.prepareStatement("select s.store_id, a.address from store s left join address a "
        		+ "on (s.address_id=a.address_id);");
        try (ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
            	stores.add(rs.getString(1)+" "+rs.getString(2));
            }
        }
        return stores;
    }
	
	public String chooseStore(List<String> stores) {
        String store = "";
        for(String s : stores) {
            System.out.println(s);
        }
        System.out.println("\nQuina botiga vols? (copia tot el text)");
        while (!stores.contains(store)){
        	store = scanner.nextLine();
            if (!stores.contains(store))
                System.out.println("Aquesta botiga no existeix.");
        }
        return store;
    }
	
	public List<String> filmsByKeyWord(String keyword, String store) throws SQLException {
		List<String> films = new ArrayList<String>();
		String data = "";
		String dispo = "";
		
		st = connection.prepareStatement(
				"select f.title, concat('botiga',i.store_id), a.first_name, c.name, "
				+ "case when r.return_date is not null "
				+ "then 'Disponible' "
				+ "else concat('Disponible a partir del: ', DATE_ADD(r.rental_date, "
				+ "INTERVAL f.rental_duration DAY)) "
				+ "end as ret_date "
				+ "from film f "
				+ "left join inventory i on (f.film_id=i.film_id) "
				+ "left join store s on (i.store_id=s.store_id) "
				+ "left join film_actor fa on(fa.film_id=f.film_id) "
				+ "left join actor a on (a.actor_id=fa.actor_id) "
				+ "left join film_category fc on(fc.film_id=f.film_id) "
				+ "left join category c on (c.category_id=fc.category_id) "
				+ "left join rental r on (r.inventory_id=i.inventory_id) "
				+ "where i.store_id = (?) and( lower(f.title) like lower(?) or lower(a.first_name) like lower(?) "
				+ "or lower(c.name) like lower(?)) "
				+ "group by f.title;");
		
		st.setString(1, store.split(" ")[0]);
		st.setString(2, "%"+keyword+"%");
		st.setString(3, "%"+keyword+"%");
		st.setString(4, "%"+keyword+"%");
		
        try (ResultSet rs = st.executeQuery()) {
            while(rs.next()) {
          	
            	films.add(rs.getString("f.title")+" "+rs.getString(2)+" "+rs.getString(5));
            }
        }
        
        return films;
    }
	
	public String chooseFilm(List<String> films) {
        String film = "";
        for(String f : films) {
            System.out.println(f);
        }
        System.out.println("\nQuina pelicula vols? ");
        while (!films.contains(film)) {
        	film = scanner.nextLine();
            if (!films.contains(film))
                System.out.println("Aquesta pelicula no existeix.");
        }
        return film;
    }
	
	public void showFilm(String film) throws SQLException{
		st = connection.prepareStatement(
				"select f.title, f.description, f.release_year, l.name, a.first_name, a.last_name "
				+ "from film f "
				+ "left join language l on(f.original_language_id=l.language_id) "
				+ "left join film_actor fa on (f.film_id=fa.film_id) "
				+ "left join actor a on (fa.actor_id=a.actor_id) "
				+ "where lower(f.title) like (?) "
				+ "group by f.title"
		);
		
		film=film.split(" ")[0]+" "+film.split(" ")[1];
		st.setString(1, "%"+film+"%");
		
        try (ResultSet rs = st.executeQuery()) {
            while(rs.next()) {
                System.out.println(rs.getString("f.title")+" "+
                    rs.getString("f.description")+" "+
                    rs.getString("f.release_year")+" "+
                    rs.getString("l.name")+" "+
                    rs.getString("a.first_name")+" "+
                    rs.getString("a.last_name")
                );
            }
        }
	}
	
	public void execute() {
        String store;
        List<String> films = new ArrayList<String>();
        
        try {
            //1- Connecta a la bbdd
            connect();
            //2- Mostra les botigues i demana a l'usuari que en tri� una
            store = chooseStore(getStores());
            System.out.println("Botiga seleccionada: "+store);
            //3- Mostra les pelicules que es troben a la botiga seleccionada
            System.out.println("Filtra pelicules amb una paraula clau: ");
            films = filmsByKeyWord(scanner.nextLine(), store);
            System.out.println("Tria una per veure els detalls: ");
            showFilm(chooseFilm(films));
            
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnect();
        }
    }
	
}
