package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Exercici10 {

	public static void main(String[] args) {
		String sql = "select f.title, i.store_id  from film f "
				+ "left join inventory i on (f.film_id=i.film_id) "
				+ "where lower(f.title) like lower(?) or lower(f.description) like lower(?) "
				+ "group by f.title;";
						
			Scanner sc = new Scanner(System.in);
			String paraulaClau=" ";
			
			try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); PreparedStatement st = conn.prepareStatement(sql)) {
			
				while (!paraulaClau.equals("")) {
			    System.out.println("Paraula Clau (en blanc per sortir): ");
			    paraulaClau = sc.nextLine();
			    	if (!paraulaClau.equals("")) {
				        st.setString(1, "%"+paraulaClau+"%");
				        st.setString(2, "%"+paraulaClau+"%");
				        
				        try (ResultSet rs = st.executeQuery()) {
				            while (rs.next()) {
				                String film = rs.getString("f.title")+" ";
				                film += rs.getString("i.store_id")+" ";
				                    System.out.println(film);
			                }
			            }
			        }
			    }
				
			} catch (SQLException e) {
			    e.printStackTrace();
			}
		        sc.close();
	}

}
