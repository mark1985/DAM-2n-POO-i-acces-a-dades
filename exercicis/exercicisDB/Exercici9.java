package exercicisDB;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Exercici9 {

	public static void main(String[] args) {
		String sql = "select p.amount, p.payment_date, ifnull(f.title,' ')as item from payment p "
				+ "left join rental r on (p.rental_id = r.rental_id) "
				+ "left join inventory i on (i.inventory_id = r.inventory_id) "
				+ "left join film f on (f.film_id = i.film_id) "
				+ "left join customer c on (p.customer_id = c.customer_id) "
				+ "where c.first_name = ? and c.last_name = ? "
				+ "and p.payment_date>= ? and p.payment_date <= ?;";
						
			Scanner sc = new Scanner(System.in);
			String lastName=" ";
			
			try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); PreparedStatement st = conn.prepareStatement(sql)) {
			
				while (!lastName.equals("")) {
			    System.out.println("Nom, Cognom i dues dates a cercar (en blanc per sortir): ");
			    lastName = sc.nextLine();
			    	if (!lastName.equals("")) {
			    		lastName.split(" ");
				        st.setString(1, lastName.split(" ")[0]);
				        st.setString(2, lastName.split(" ")[1]);
				        st.setDate(3, Date.valueOf(lastName.split(" ")[2]));
				        st.setDate(4, Date.valueOf(lastName.split(" ")[3]));
				        try (ResultSet rs = st.executeQuery()) {
				            while (rs.next()) {
				                String film = rs.getString("p.amount")+" ";
				                film += rs.getString("p.payment_date")+" ";
				                film += rs.getString("item");
				                    System.out.println(film);
			                }
			            }
			        }
			    }
				
			} catch (SQLException e) {
			    System.err.println("Error SQL: "+e.getMessage());
			}
		        sc.close();

	}

}
