package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exercici2 {

	public static void main(String[] args) {
		
		String sql = "select first_name, last_name from actor where actor_id in"
						+ "(select actor_id from film_actor where film_id in"
						+ "(select film_id from film where title like('TWISTED PIRATES')));";
		
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); Statement st = con.createStatement()){
					System.out.println("Connexio OK");
					System.out.println();
					
				try(ResultSet rs = st.executeQuery(sql)){
					
					while(rs.next()){
						System.out.println(rs.getString("first_name")+" "+rs.getString("last_name"));
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}

}
