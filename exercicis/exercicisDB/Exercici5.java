package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exercici5 {

	public static void main(String[] args) {
				
		String sql = "select c.first_name, c.last_name, a.phone, f.title from rental r "
				+ "left join inventory i on (r.inventory_id=i.inventory_id) "
				+ "left join film f on (f.film_id=i.film_id) "
				+ "left join customer c on (c.customer_id=r.customer_id) "
				+ "left join address a on (c.address_id=a.address_id) "
				+ "where r.return_date is null and r.rental_date+f.rental_duration < now();";

		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); Statement st = con.createStatement()){
					System.out.println("Connexio OK");
					System.out.println();
					
				try(ResultSet rs = st.executeQuery(sql)){
					
					while(rs.next()){
						System.out.println(rs.getString("c.first_name")+" "+rs.getString("c.last_name")
						+" "+rs.getString("a.phone")+" "+rs.getString("f.title"));
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}

	}

}
