package model;

import java.util.Random;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Player {
	private static Random random = new Random();
	private IntegerProperty hiddenCoins = new SimpleIntegerProperty();
	private IntegerProperty bet = new SimpleIntegerProperty();
	
	public int getHiddenCoins() {
		return hiddenCoins.get();
	}
	
	public IntegerProperty getHiddenCoinsProperty() {
		return hiddenCoins;
	}
	
	public void setHiddenCoins(int hiddenCoins) {
		if (hiddenCoins < 0 || hiddenCoins > 3)
			throw new IllegalArgumentException("Quantitat de monedes inv�lida");
		this.hiddenCoins.set(hiddenCoins);
	}
	
	public void hide() {
		hiddenCoins.set(random.nextInt(4));
	}
	
	public int getBet() {
		return bet.get();
	}
	
	public IntegerProperty getBetProperty() {
		return bet;
	}
	
	public void setBet(int bet) {
		if (bet < 0)
			throw new IllegalArgumentException("L'aposta ha de ser positiva");
		this.bet.set(bet);
	}
	
	public void bid() {
		int numMonedes = random.nextInt(Game.N_PLAYERS*3+1);
		bet.set(numMonedes);
	}
}
