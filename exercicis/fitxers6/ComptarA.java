package fitxers6;

import java.io.FileReader;
import java.io.IOException;

public class ComptarA {

	public static void main(String[] args) {
		String nomFitxer = "ComptarA.txt";
        int lectura;
        char ch;
        int cont = 0;
        
        try (FileReader lector = new FileReader(nomFitxer)) {
            while ((lectura = lector.read())!=-1) {
                ch = (char) lectura;
                if (ch == 'a'|| ch == 'A') {
                	cont++;
                }
            }
            
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        
        System.out.println("El fitxer contenia "+cont+" lletres A");

	}

}
