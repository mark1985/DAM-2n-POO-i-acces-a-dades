package exercicis3Java;

import java.util.ArrayList;
import java.util.Random;

public class Tirada implements Comparable<Tirada>, Cloneable{
	private int atac;
	private int defensa;
	private ArrayList<Integer> tirada;
	
	private Random rand;
	
	public Tirada(int atac, int defensa, int numTiradas){
		this.atac=atac;
		this.defensa=defensa;
		this.tirada=new ArrayList<Integer>();
		this.rand = new Random();
		
		for (int i=0; i<numTiradas; i++){
			this.tirada.add(i,(rand.nextInt(10)+1));
		}
		
		this.tirada.sort(null);
		
	}
	
	private int afegirZero(Tirada objeto){
		int control=-1;
		
		if (this.tirada.size()==objeto.tirada.size()){
			control = 1;
		}
		else if (this.tirada.size()>objeto.tirada.size()){
			objeto.tirada.add(0,0);
		}
		else if (this.tirada.size()<objeto.tirada.size()){
			this.tirada.add(0,0);
		}
		return control;
	}
	
	public String toString(){
		return ("AT: "+String.valueOf(this.atac)+" DEF: "+String.valueOf(this.defensa)+
				" Daus("+String.valueOf(this.tirada.size())+"): "+String.valueOf(this.tirada));
	}
	
	public int getAtac() {
		return atac;
	}

	public int getDefensa() {
		return defensa;
	}

	public ArrayList<Integer> getTirada() {
		return tirada;
	}

	@Override
	public int compareTo(Tirada objeto) {
		int marcador1=0, marcador2=0;
		
		while(this.afegirZero(objeto)==-1);
		
		for(int i=0;i<this.tirada.size();i++){
			if((this.tirada.get(i)+this.atac)>(objeto.tirada.get(i)+objeto.defensa)){
				marcador1++;
			}
			if((objeto.tirada.get(i)+objeto.atac)>(this.tirada.get(i)+this.defensa)){
				marcador2++;
			}
		}
		
		return marcador1-marcador2;
	}
	
	@Override
	public Tirada clone(){
		try {
           return (Tirada) super.clone();
       }catch (CloneNotSupportedException e) {
           // Mai ha d'arribar aqu�
           e.printStackTrace();
           return null;
       }
	}
}

