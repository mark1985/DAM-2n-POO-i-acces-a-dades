package exercicis3Java;

public class MainTirada {

	public static void main(String[] args) {
		
		Tirada jugador1 = new Tirada(3,1,5);
		Tirada jugador2 = new Tirada(2,2,2);
				
		System.out.println("Jugador1 "+jugador1.toString());
		System.out.println("Jugador2 "+jugador2.toString());
		
		System.out.println("Comparacio de les dues tirades: "+jugador1.compareTo(jugador2));	
		System.out.println("Comparació d'una tirada amb una d'igual: "+jugador1.compareTo(jugador1.clone()));
	}

}
