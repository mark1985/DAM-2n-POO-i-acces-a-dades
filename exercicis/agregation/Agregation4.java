package agregation;

import java.util.Arrays;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;

public class Agregation4 {
	
	private Alumne alumne;
	
	public static void main(String[] args) {
		new Agregation4().start();
	}
	
	public void start(){

		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("students");
		MongoCollection<Document> coll = db.getCollection("students");
			
		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.group(new Document("alumne", "$_id").append("nom", "$name")
						.append("prova", "$scores.type"),Accumulators.avg("mitjana","$scores.score")),
				Aggregates.sort(Sorts.ascending("_id.alumne"))
			)).forEach((Document doc)->{
				
					Document id = doc.get("_id", Document.class);
					
					if (alumne==null || alumne.id != id.getInteger("alumne")) {
						if (alumne!=null){
							System.out.println(alumne.toString());
						}

						alumne = new Alumne(id.getInteger("alumne"), id.getString("nom"));
					}
					
					String tipo = id.getString("prova");
					if (tipo.equals("quiz")){
						alumne.notaQuiz=doc.getDouble("mitjana");
					}
					else if (tipo.equals("homework")){
						alumne.notaHw=doc.getDouble("mitjana");
					}
					else if (tipo.equals("exam")) {
						alumne.notaExam=doc.getDouble("mitjana");
					}
					
				});
			System.out.println(alumne.toString());
		client.close();

	}
}
