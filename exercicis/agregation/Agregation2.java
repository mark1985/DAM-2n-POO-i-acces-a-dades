package agregation;

import java.util.Arrays;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;

public class Agregation2 {
	
	private static int anterior = -1;

	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("students");
		MongoCollection<Document> coll = db.getCollection("students");
		
		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.group(new Document("alumne", "$_id").append("nom", "$name")
						.append("prova", "$scores.type"),Accumulators.sum("count",1)),
				Aggregates.sort(Sorts.ascending("_id.nom","_id.alumne"))
			)).forEach((Document doc)->{
				Document alumne = doc.get("_id", Document.class);
				
				if(anterior!=alumne.getInteger("alumne")){
					anterior=alumne.getInteger("alumne");
					System.out.println("Alumne: "+alumne.getString("nom"));
				}
				
				System.out.println(" prova:"+alumne.getString("prova")+" count:"+doc.getInteger("count"));
			
		});
			
		client.close();
	}

}
