package agregation;

public class Alumne {
	public final String nom;
	public final int id;
	public double notaQuiz;
	public double notaHw;
	public double notaExam;
	
	public Alumne(int id, String name) {
		this.id = id;
		this.nom = name;
	}
	
	public double getNotaFinal() {
		double notaFinal = 0.1*notaQuiz + 0.3*notaHw + 0.6*notaExam;
		if (notaExam<40 && notaFinal>40){
			notaFinal=40;
		}
		
		return notaFinal;
	}
	
	@Override
	public String toString() {
		return "Alumne: ("+id+") "+nom+" - Nota final: "+getNotaFinal();
	}
	
	public String getProves() {
		return "Exam: ("+notaExam+") "+"Quiz: ("+notaQuiz+") "+"HomeWork: ("+notaHw+") ";
	}
}
