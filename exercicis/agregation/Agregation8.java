package agregation;

import java.util.Arrays;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;

public class Agregation8 {
	
	private static int anterior = -1;
	
	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("grades");
		MongoCollection<Document> coll = db.getCollection("grades");

		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.group(new Document("classe", "$class_id").append("alumne", "$student_id")),
				Aggregates.sort(Sorts.ascending("_id.classe", "_id.alumne"))
			)).forEach((Document doc) -> {
				
				Document clase = doc.get("_id", Document.class);

				if(anterior!=clase.getInteger("classe")){
					anterior=clase.getInteger("classe");
					System.out.println("Classe: "+clase.getInteger("classe"));
				}
				
				System.out.println("Codi alumne: "+clase.getInteger("alumne"));
				
			});
		
		client.close();

	}

}
