package agregation;

import java.util.Arrays;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;

public class Agregation1 {

	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("students");
		MongoCollection<Document> coll = db.getCollection("students");

		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.match(Filters.eq("scores.type","exam")),
				Aggregates.group("$name", Accumulators.avg("nota", "$scores.score")),
				Aggregates.match(Filters.gte("nota", 40))
			)).forEach((Document doc) -> {
				System.out.println(doc.getString("_id")+" ("+doc.getDouble("nota")+")");
		});
		
		client.close();
	}

}
