package agregation;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Agregation6 {
	
	private static int anterior = -1;
	
	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("grades");
		MongoCollection<Document> coll = db.getCollection("grades");
		
		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.match(Filters.eq("scores.type","exam")),
				Aggregates.group(new Document("classe", "$class_id"), Accumulators.avg("mitjana", "$scores.score")),
				Aggregates.sort(Sorts.ascending("_id.classe"))
			)).forEach((Document doc)->{
				Document clase = doc.get("_id", Document.class);
				
				if(anterior!=clase.getInteger("classe")){
					anterior=clase.getInteger("classe");
					System.out.println("Classe: "+clase.getInteger("classe"));
				}
				
				System.out.println(" mitjana:"+doc.getDouble("mitjana"));
		});
			
		client.close();

	}

}
