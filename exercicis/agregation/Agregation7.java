package agregation;

import java.util.Arrays;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Agregation7 {
	
	private static int anterior = -1;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Escriu la id de l'alumne que busques");
		int nom = sc.nextInt();
		System.out.println();
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("grades");
		MongoCollection<Document> coll = db.getCollection("grades");

		coll.aggregate(Arrays.asList(
				Aggregates.unwind("$scores"),
				Aggregates.match(Filters.eq("student_id",nom)),
				Aggregates.group(new Document("alumne", "$student_id")
						.append("prova", "$scores.type"),Accumulators.avg("mitjana","$scores.score")),
				Aggregates.sort(Sorts.ascending("_id.alumne"))
			)).forEach((Document doc) -> {
				
				Document id = doc.get("_id", Document.class);

				if(anterior!=id.getInteger("alumne")){
					anterior=id.getInteger("alumne");
					System.out.println("Alumne: "+id.getInteger("alumne"));
				}
				
				System.out.println(id.getString("prova")+" mitjana:"+doc.getDouble("mitjana"));
			});
		
		sc.close();
		client.close();

	}

}
