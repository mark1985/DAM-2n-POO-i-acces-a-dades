package exercici7Llistes;

import java.util.ArrayList;
import java.util.List;

public class LlistaOrdenada {
	private List<Integer> llista = new ArrayList<Integer>();
	
	public LlistaOrdenada(){
		
	}
	
	public void suprimirElement(int pos){
		this.llista.remove(pos);
		this.llista.sort(null);
	}
	
	public void afegirElement(Integer elem){
		this.llista.add(elem);
		this.llista.sort(null);
	}
	
	public void cercaSuprimirElement(Integer elem){
		this.llista.remove(cerca(elem));
	}
	
	public void cercaAfegirElement(Integer elem){
		this.llista.add(cerca(elem), elem);
	}
	
	public int cerca(Integer elem){
		
		int end = 0;
		int index = 0;
		Integer num = 0;
		boolean control = false;
		int result = -1;
		
		end = this.llista.size();
		index = end/2;
		num = this.llista.get(index);
		
		while(control == false){
			
			if(!elem.equals(num)){
				if(elem > num){
					index=(index + end)/2;
				}
				else if(elem < num){
					end = index;
					index = end/2;
				}
				num = this.llista.get(index);
				
				if(end - index <= 1){
					
					if(num.equals(elem)){
						result = index;
						control = true;
					}
					else if(this.llista.get(end).equals(elem)){
						result = end;
						control = true;
					}
					else if(elem>this.llista.get(index)&&elem<this.llista.get(index+1)){
						result=index+1;
						control = true;
					}
					else{
						result = -1;
					}
				}
			}
			
			else{
				control = true;
				result = index;
			}
				
		}
		
		return result;
	}
	
	public Integer getNumero(int pos){
		return this.llista.get(pos);
	}
	
	@Override
	public String toString(){
		String cadena = new String();
		
		for(int i=0; i<this.llista.size(); i++){
			cadena+=i+".- "+this.llista.get(i)+"\n";
		}
		
		return cadena;
	}

}
