package exercici7Llistes;

import java.util.Random;

public class cercaBinMain {

	public static void main(String[] args) {
		LlistaOrdenada llista = new LlistaOrdenada();
		Integer num;
		Integer busca;
		Random rand = new Random();
		
		num = 10;
		
		for(int i=0; i<20; i++){
			num = ((num/2)*8)/3;
			llista.afegirElement(num);
		}
		
		busca = llista.getNumero(rand.nextInt(20));
		System.out.println(llista.toString());
		
		System.out.println("El numero "+busca+" es troba a la posicio "+llista.cerca(busca));
		
		llista.cercaAfegirElement(1024);
		
		System.out.println(llista.toString());
		
		llista.cercaSuprimirElement(1024);
		llista.suprimirElement(0);
		System.out.println(llista.toString());
	}

}
