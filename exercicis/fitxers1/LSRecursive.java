package fitxers1;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LSRecursive {

	public static void main(String[] param) {
		List<Path> subdirectoris = new ArrayList();
		String permisos = new String();
		int cont;
		
		if(param.length==1){
			Path directori;
			
			subdirectoris.add(Paths.get(param[0]));
			cont=0;
			while(cont<subdirectoris.size()){
				directori=subdirectoris.get(cont);
				
				System.out.println("Fitxers del directori "+directori);
				
				try(DirectoryStream<Path> llista = Files.newDirectoryStream(directori)){
					
					for (Path fitxer : llista){
						permisos="";
						
						if(Files.isDirectory(fitxer)){
							permisos+="d";
							
							if(!subdirectoris.contains(fitxer)){
								subdirectoris.add(fitxer);
							}
							
							if(Files.isReadable(fitxer)){
								permisos+="r";
							}
							else{
								permisos+="-";
							}
							if(Files.isWritable(fitxer)){
								permisos+="w";
							}
							else{
								permisos+="-";
							}
							if(Files.isExecutable(fitxer)){
								permisos+="x";
							}
							else{
								permisos+="-";
							}
								
							System.out.println(permisos+" "+fitxer.getFileName());
						}
						else{
							permisos+="-";
							
							if(Files.isReadable(fitxer)){
								permisos+="r";
							}
							else{
								permisos+="-";
							}
							if(Files.isWritable(fitxer)){
								permisos+="w";
							}
							else{
								permisos+="-";
							}
							if(Files.isExecutable(fitxer)){
								permisos+="x";
							}
							else{
								permisos+="-";
							}
								
							System.out.println(permisos+" "+fitxer.getFileName());
						}
					}
					cont++;
					
				} catch (IOException e) {
					System.err.println(e);
				}
			}
				
		}
			
		else{
			System.err.println("Nomes pots introduir un parametre");
		}
	}

}
