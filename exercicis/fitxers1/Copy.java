package fitxers1;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Copy {

	public static void main(String[] param) {
		
		if(param.length==2){
			Path copiable = Paths.get(param[0]);
			Path target = Paths.get(param[1]);
			System.out.println("Copiant el fitxer "+copiable+" a "+target);
								
			try {
				Files.copy(copiable, target);
			} catch (IOException e) {

				e.printStackTrace();
			}

		}
			
		else{
			System.err.println("Nomes pots introduir un parametre");
		}
			
	}

}
