package fitxers1;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SimpleLS {

	public static void main(String[] param) {
		String permisos = new String();
		if(param.length==1){
			Path directori = Paths.get(param[0]);
			System.out.println("Fitxers del directori "+directori);
			
				try(DirectoryStream<Path> llista = Files.newDirectoryStream(directori)){
					
					for (Path fitxer : llista){
						permisos="";
						
						if(Files.isDirectory(fitxer)){
							permisos+="d";
							
							if(Files.isReadable(fitxer)){
								permisos+="r";
							}
							else{
								permisos+="-";
							}
							if(Files.isWritable(fitxer)){
								permisos+="w";
							}
							else{
								permisos+="-";
							}
							if(Files.isExecutable(fitxer)){
								permisos+="x";
							}
							else{
								permisos+="-";
							}
								
							System.out.println(permisos+" "+fitxer.getFileName());
						}
						else{
							permisos+="-";
							
							if(Files.isReadable(fitxer)){
								permisos+="r";
							}
							else{
								permisos+="-";
							}
							if(Files.isWritable(fitxer)){
								permisos+="w";
							}
							else{
								permisos+="-";
							}
							if(Files.isExecutable(fitxer)){
								permisos+="x";
							}
							else{
								permisos+="-";
							}
								
							System.out.println(permisos+" "+fitxer.getFileName());
						}
					}
					
				} catch (IOException e) {
					System.err.println(e);
				}
		}
			
		else{
			System.err.println("Nomes pots introduir un parametre");
		}
			
	}

}
