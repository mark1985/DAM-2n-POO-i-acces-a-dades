package exercicis5Java;

public class ScrabbleEx implements Comparable<ScrabbleEx>{
	private Casella tablero[];
	
	public ScrabbleEx (Casella vector[]){
		this.tablero = new Casella[vector.length];
		
		for(int i=0;i<vector.length;i++){
			this.tablero[i] = new Casella();
		}
		
	}
	
	public int valor(){		
		int valor = 0;
		boolean control = false;
		int multiplicador = 0;
		
		for(int i=0;i<this.tablero.length;i++){
			
			if(this.tablero[i].getModTipus()=="palabra"){
				control=true;
				multiplicador = this.tablero[i].getMods();
			}
			
			switch(this.tablero[i].getLetra()){
				case 'A':
				case 'E':
				case 'I':
				case 'L':
				case 'N':
				case 'O':
				case 'R':
				case 'S':
				case 'T':
				case 'U':	if(this.tablero[i].getMods()!=0 && (this.tablero[i].getModTipus()=="letra")){
								valor = valor +(1 * this.tablero[i].getMods());
							}
					break;
				case 'D':
				case 'G':	if(this.tablero[i].getMods()!=0 && (this.tablero[i].getModTipus()=="letra")){
								valor = valor +(2 * this.tablero[i].getMods());
							}
					break;
				case 'B':
				case 'C':
				case 'M':
				case 'P':	if(this.tablero[i].getMods()!=0 && (this.tablero[i].getModTipus()=="letra")){
								valor = valor +(3 * this.tablero[i].getMods());
							}
					break;
				case 'F':
				case 'H':
				case 'V':
				case 'W':
				case 'Y':	if(this.tablero[i].getMods()!=0 && (this.tablero[i].getModTipus()=="letra")){
								valor = valor +(4 * this.tablero[i].getMods());
							}
					break;
				case 'K':	if(this.tablero[i].getMods()!=0 && (this.tablero[i].getModTipus()=="letra")){
								valor = valor +(5 * this.tablero[i].getMods());
							}
					break;
				case 'J':
				case 'X':	if(this.tablero[i].getMods()!=0 && (this.tablero[i].getModTipus()=="letra")){
								valor = valor +(8 * this.tablero[i].getMods());
							}
					break;
				case 'Q':
				case 'Z':	if(this.tablero[i].getMods()!=0 && (this.tablero[i].getModTipus()=="letra")){
								valor = valor +(10 * this.tablero[i].getMods());
							}
					break;
			}
		}
		
		if(control==true){
			return valor * multiplicador;
		}
		else{
			return valor;
		}
		
	}

	@Override
	public int compareTo(ScrabbleEx paraula) {
		return this.valor()-paraula.valor();
	}

	public Casella[] getTablero() {
		return tablero;
	}

	public void setTablero(String paraula) {
		
		for(int i=0;i<paraula.length();i++){
			this.tablero[i].setLetra(paraula.charAt(i));
			
			if(i%3==0){
				this.tablero[i].setModTipus("letra", 3);
			}
			else if(i%4==0){
				this.tablero[i].setModTipus("palabra", 3);
			}
		}
		
	}
}
