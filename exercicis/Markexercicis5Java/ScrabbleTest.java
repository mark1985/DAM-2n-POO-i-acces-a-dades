package exercicis5Java;

public class ScrabbleTest {

	public static void main(String[] args) {
		
		Scrabble vectorScrabble[] = new Scrabble[2];
		String vectorString[] = new String[2];
		Casella vectorCasella[] = new Casella[25];
		ScrabbleEx objScrabbleEx = new ScrabbleEx(vectorCasella);
		Casella vectorCasella2[] = new Casella[25];
		ScrabbleEx objScrabbleEx2 = new ScrabbleEx(vectorCasella2);
		
		vectorScrabble[0]= new Scrabble ("animal");
		vectorScrabble[1]= new Scrabble ("ornitorrinco");
		
		System.out.println(vectorScrabble[0].toString());
		System.out.println(vectorScrabble[1].toString());
		
		System.out.println(vectorScrabble[0].compareTo(vectorScrabble[1]));
		System.out.println();
		
		vectorString[0]= new String ("perruno");
		vectorString[1]= new String ("gatuno");
		
		System.out.println(vectorString[0]+" "+ComparadorScrabble.getInstance().valor(vectorString[0]));
		System.out.println(vectorString[1]+" "+ComparadorScrabble.getInstance().valor(vectorString[1]));
		System.out.println(ComparadorScrabble.getInstance().compare(vectorString[0], vectorString[1]));
		System.out.println();
		
		
		objScrabbleEx.setTablero("animal");
		objScrabbleEx2.setTablero("otorrinolaringologo");
		
		for(int i=0;i<objScrabbleEx.getTablero().length;i++){
			System.out.format("%c",objScrabbleEx.getTablero()[i].getLetra());
		}
		System.out.println();
		for(int i=0;i<objScrabbleEx.getTablero().length;i++){
			System.out.format("%c",objScrabbleEx2.getTablero()[i].getLetra());
		}
		System.out.println();	
		System.out.println(objScrabbleEx.compareTo(objScrabbleEx2));
	}
}
