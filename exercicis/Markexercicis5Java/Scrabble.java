package exercicis5Java;

public class Scrabble implements Comparable<Scrabble>{
	private int valor = 0;
	private String palabra;
	
	public Scrabble(String paraula){
		boolean control = false;
		
		for(int i=0;i<paraula.length();i++){
			if(!Character.isLetter(paraula.charAt(i))){
				control = false;
				throw new IllegalArgumentException("La cadena cont� espais o simbols");
			}
			control = true;
		}
		
		if (control == true){
			this.palabra = paraula;
			this.valor = this.valor();
		}
	}
	
	public int valor(){		
		int valor = 0;
		this.palabra = this.palabra.toUpperCase();
		
		for(int i=0;i<this.palabra.length();i++){
			
			switch(this.palabra.charAt(i)){
				case 'A':
				case 'E':
				case 'I':
				case 'L':
				case 'N':
				case 'O':
				case 'R':
				case 'S':
				case 'T':
				case 'U':	valor+=1;
					break;
				case 'D':
				case 'G':	valor+=2;
					break;
				case 'B':
				case 'C':
				case 'M':
				case 'P':	valor+=3;
					break;
				case 'F':
				case 'H':
				case 'V':
				case 'W':
				case 'Y':	valor+=4;
					break;
				case 'K':	valor+=5;
					break;
				case 'J':
				case 'X':	valor+=8;
					break;
				case 'Q':
				case 'Z':	valor+=10;
					break;
			}
		}
		return valor;
	}

	@Override
	public int compareTo(Scrabble paraula) {
		return this.valor()-paraula.valor();
	}
	
	@Override
	public String toString(){
		return this.palabra+" "+String.valueOf(this.valor());
		
	}

}
