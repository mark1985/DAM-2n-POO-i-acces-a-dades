package exercicis5Java;

import java.util.Comparator;

public class ComparadorScrabble implements Comparator<String>{
	
	static ComparadorScrabble COMPARADOR_SCRABBLE=null;
	
	private ComparadorScrabble(){
		
	}

	@Override
	public int compare(String p1, String p2) {
		return valor(p1)-valor(p2);
	}
	
	public int valor(String pal){		
		int valor = 0;
		pal = pal.toUpperCase();
		
		for(int i=0;i<pal.length();i++){
			
			switch(pal.charAt(i)){
				case 'A':
				case 'E':
				case 'I':
				case 'L':
				case 'N':
				case 'O':
				case 'R':
				case 'S':
				case 'T':
				case 'U':	valor+=1;
					break;
				case 'D':
				case 'G':	valor+=2;
					break;
				case 'B':
				case 'C':
				case 'M':
				case 'P':	valor+=3;
					break;
				case 'F':
				case 'H':
				case 'V':
				case 'W':
				case 'Y':	valor+=4;
					break;
				case 'K':	valor+=5;
					break;
				case 'J':
				case 'X':	valor+=8;
					break;
				case 'Q':
				case 'Z':	valor+=10;
					break;
			}
		}
		return valor;
	}
	
	public static ComparadorScrabble getInstance(){
		
		if(COMPARADOR_SCRABBLE==null){
			COMPARADOR_SCRABBLE = new ComparadorScrabble();
		}
		
		return COMPARADOR_SCRABBLE;
		
	}

}
