package exercicis5Java;

public class Casella implements Cloneable{
	private char letra;
	private int mods = 0;
	private String modTipus;
	
	public Casella(){
		
	}
	
	public String getModTipus() {
		return modTipus;
	}

	public void setModTipus(String modTipus, int mod) {
		if(modTipus.equalsIgnoreCase("letra")){
			this.modTipus = modTipus;
			this.mods = mod;
		}
		else if(modTipus.equalsIgnoreCase("palabra")){
			this.modTipus = modTipus;
			this.mods = mod;
		}
		else{
			throw new IllegalArgumentException("El modificador ha de ser 'letra' o 'palabra'");
		}
			
	}

	public char getLetra() {
		return letra;
	}

	public void setLetra(char letra) {
		this.letra = Character.toUpperCase(letra);
	}

	public int getMods() {
		return mods;
	}
}
