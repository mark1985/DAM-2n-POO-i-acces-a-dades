package fitxers3;

import java.io.Serializable;
import java.util.Random;

abstract class Planta implements Serializable{
	
	private boolean esViva=true;
	private int altura=1;
	
	public Planta(){
		
	}
	
	abstract char getChar(int nivell);
	
	public Planta creix(){
		if (this.altura<10){
			this.altura++;
		}
		if (this.altura==10){
			this.esViva=false;
		}
		
		return null;
	}
	
	public int escampaLlavor(){
		Random rand = new Random();
		
		return rand.nextInt(5)-3+1;
	}
	
	public int getAltura(){
		return this.altura;
	}
	public boolean getEsViva(){
		return this.esViva;
	}
	public void setAltura(int a){
		this.altura=a;
	}
	public void setEsViva(boolean bool){
		this.esViva=bool;
	}
	public boolean esViva(){
		if (this.esViva==true){
			return true;
		}
		else{
			return false;
		}
	}
}
