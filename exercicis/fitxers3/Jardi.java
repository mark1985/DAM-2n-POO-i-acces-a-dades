package fitxers3;

import java.io.Serializable;

public class Jardi implements Serializable{
	
	Planta jardi[];
	int midaJardi;
	
	public Jardi(int a){
		if(a<=0){
			a=10;
		}
		this.jardi=new Planta[a];
		this.midaJardi=a;
	}
	
	public void temps(){
		int escampa=0;
		Planta planta;
		
		for(int i=0;i<this.midaJardi;i++){			
			
			if(this.jardi[i]!=null){
				
				planta=this.jardi[i].creix();
				
				while(escampa==0){
					escampa = jardi[i].escampaLlavor();
				}
				
				if(planta instanceof Planta){
					
					if(planta instanceof Llavor){
						if(((i+escampa)<midaJardi && (i+escampa)>0) && jardi[i+escampa]==null){
							jardi[i+escampa]=planta;
						}
					}
					else{
						jardi[i]=planta;
					}
				}

				if(jardi[i].getEsViva()==false){
					jardi[i]=null;
				}
			}
		}
	}
	
	public String toString(){
		String aux = new String();		
		String dibujo = new String();
		
		for(int a=10;a>=0;a--){
			for(int i=0;i<this.midaJardi;i++){
				
				if(a==0){
					aux="_";
				}
				else if(jardi[i] != null ){
					aux = String.valueOf(jardi[i].getChar(a));
				}
				else{
					aux=" ";
				}
				dibujo=dibujo+aux;
			}
			dibujo=dibujo+"\n";
		}
		return dibujo;
	}
	
	public boolean plantaLlavor(Planta novaPlanta, int pos){
		if (this.jardi[pos]==null && pos<=this.midaJardi){
			this.jardi[pos]=novaPlanta;
			return true;
		}
		else{
			return false;
		}
		
	}
}
