package fitxers3;

import java.io.Serializable;

public class Llavor extends Planta implements Serializable{
	private Planta planta;
	private int temps=0;
	
	public Llavor(Planta o){
		if(o instanceof Planta){
			this.planta = o;
		}
		else{
			throw new IllegalArgumentException("Una llavor no pot produir una altra");
		}
		
	}
	public Planta creix(){
		if (this.temps<5){
			temps++;
			return null;
		}
		else{
			return this.planta;
		}
	}
	
	@Override
	public char getChar(int nivell){
		if(this.planta.getAltura()==nivell){
			return '.';
		}
		else{
			return ' ';
		}
	}
	
	

}
