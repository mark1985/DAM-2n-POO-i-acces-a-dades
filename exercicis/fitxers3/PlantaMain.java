package fitxers3;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import javax.swing.KeyStroke;

public class PlantaMain {

	public static void main(String[] args) throws InterruptedException {
		Planta altibus = new Altibus();
		Planta declinus = new Declinus();
		Jardi jardi = new Jardi(15);
		jardi.plantaLlavor(altibus, 2);
		jardi.plantaLlavor(declinus, 8);
		String opcion = new String();
		
		Scanner scanIn = new Scanner(System.in);
		
		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("jardi.bin"));) {
             
			jardi = (Jardi) lector.readObject();

        } catch (IOException ex) {
            System.err.println(ex);
        } catch (ClassNotFoundException ex) {
            System.err.println(ex);
        }
		
		while (!opcion.equals("n")){
			jardi.temps();
			System.out.format(" %s ",jardi.toString());
			System.out.println("Seguir? s/n");
			opcion = scanIn.nextLine();
		}
		
		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("jardi.bin"))){
                escriptor.writeObject(jardi);
        } catch (IOException ex) {
            System.err.println(ex);
        }
		
	}

}
