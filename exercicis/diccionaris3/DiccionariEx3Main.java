package diccionaris3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DiccionariEx3Main {

	public static void main(String[] args) {
		Map<String,String> llistaPaisos = new HashMap<String,String>();
		String pais = new String();
		String capital = new String();
		
		llistaPaisos.put("Espanya", "Madrid");
		llistaPaisos.put("Francia", "Paris");
		llistaPaisos.put("Italia", "Roma");
		llistaPaisos.put("Anglaterra", "Londres");
		llistaPaisos.put("Alemanya", "Berlin");
		
		Scanner scan = new Scanner(System.in);
		
		do{
			System.out.println("Introdueix un nom de pa�s:");
			pais = scan.nextLine();
			
			if(!pais.equals("")){
				pais = capitalize(pais);
				
				if(llistaPaisos.containsKey(pais)){
					System.out.println("La capital de "+pais+" es "+llistaPaisos.get(pais));
				}
				else{
					
					System.out.println("Quina es la capital de "+pais+"?");
					capital = scan.nextLine();
										
					if(!capital.equals("")){
						capital = capitalize(capital);
						llistaPaisos.put(pais,capital);
					}
				}
			}
			
		}while(!pais.equals(""));
		
		scan.close();
	}
	
	public static String capitalize (String cadena){
		char[] str = new char [cadena.length()];
		
		cadena.toLowerCase();
		
		for(int i=0; i<cadena.length(); i++){
			str[i] = cadena.charAt(i);
		}
		
		str[0] = Character.toUpperCase(str[0]);
		
		cadena = "";
		
		for(int i=0; i<str.length; i++){
			cadena+=str[i];
		}
		
		return cadena;
	}

}
