package fitxers11;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class Xml1 {
	
	public static int MAX = 5;
	public static String FITXER = "persones";

	public static void main(String[] args) {
		
		try {
			crearBinari();
			crearXML();
			mostraXML();
			generaHTML();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static List<Persona> crearPersones(){
		List<Persona>individus = new ArrayList<Persona>();
		individus.add(new Persona("Pere", 53));
		individus.add(new Persona("Joan", 26));
		individus.add(new Persona("Maria", 32));
		individus.add(new Persona("Laura", 19));

		return individus;
	}
	
	public static void crearBinari(){
		List<Persona>individus = crearPersones();
		
		try(DataOutputStream writer = new DataOutputStream(new FileOutputStream(FITXER+".bin"))){
			
			for (int i=0;i<individus.size();i++){
					
				writer.writeChars(individus.get(i).getNom());
				writer.writeInt(individus.get(i).getEdat());
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void crearXML(){
		List<Persona> persones = new ArrayList<Persona>();
				
		Document document = null;
		try {
			document = DocumentBuilderFactory.newInstance().newDocumentBuilder().getDOMImplementation().createDocument(null, "persones", null);
		} catch (DOMException e1) {
			e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		}
		document.setXmlVersion("1.0");
		
		/*try(DataInputStream lector = new DataInputStream(new FileInputStream(FITXER+".bin"))){	
			
			int k=0;
			char letra;
			int num;
			
			while(true){
				letra=(char) lector.readByte();
				num = (int) lector.readInt();
					
					if(Character.isLetter(letra)==true){
						param[k]=""+param[k]+letra;
					}
					else{
						param2[k]=0+num;
						k++;
					}
			}
			
		} catch (EOFException e) {
			System.err.println("final");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(int i=0;i<MAX;i++){
			persones.add(new Persona(param[i],param2[i]));
		}*/
		
//**********************************************************************************************
//NO HE TINGUT NASSOS DE FER FUNCIONAR EL LECTOR I QUE EM LLEGIS BE EL BINARI PER DESPRES GENERAR EL XML RAO PER LA QUAL HE OPTAT PER TRACTAR-HO COM A OBJECTE AL FINAL
		
		persones=crearPersones();
		
		for (Persona persona : persones) {
			Element elementPersona = document.createElement("persona");
			document.getDocumentElement().appendChild(elementPersona);

			creaElement("nom", persona.getNom(), elementPersona, document);
			creaElement("edat", String.valueOf(persona.getEdat()), elementPersona, document);
		}
		
		try {
			TransformerFactory.newInstance().newTransformer().transform(new DOMSource(document),new StreamResult(FITXER+".xml"));
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
	}
	
	public static void creaElement(String dadaPersona, String valor, Element elementPersona, Document document) {
		Element elem = document.createElement(dadaPersona);
		Text text = document.createTextNode(valor);
		elementPersona.appendChild(elem);
		elem.appendChild(text);
	}
	
	public static void mostraXML() throws IOException{
		Document document;
		try {
			document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(FITXER+".xml");
			
			System.out.println(document.getDocumentElement().getNodeName());
			NodeList persones = document.getElementsByTagName("persona");

			for (int i = 0; i < persones.getLength(); i++) {
				Node persona = persones.item(i);
				if (persona.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) persona;

					System.out.print("   " + getNode("nom", element));
					System.out.println(" \t" + getNode("edat", element));
				}
			}
		} catch (SAXException e) {
			throw new IOException(e);
		} catch (ParserConfigurationException e) {
			throw new IOException(e);
		}
	}
	
	private static String getNode(String etiqueta, Element elem) {
		NodeList node = elem.getElementsByTagName(etiqueta).item(0)
				.getChildNodes();
		Node valorNode = (Node) node.item(0);
		return valorNode.getNodeValue();
	}

	public static void generaHTML() throws IOException {
		String fullEstil = FITXER+".xsl";
		String dadesPersones = FITXER+".xml";
		
		try (FileOutputStream fos = new FileOutputStream(FITXER+".html")) {
			Source estils = new StreamSource(fullEstil);
			Source dades = new StreamSource(dadesPersones);
			Result resultat = new StreamResult(fos);
			Transformer megatron = TransformerFactory.newInstance().newTransformer(estils);
			megatron.transform(dades, resultat);
			
		} catch (TransformerConfigurationException e) {
			throw new IOException(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new IOException(e);
		} catch (TransformerException e) {
			throw new IOException(e);
		}
	}

}
