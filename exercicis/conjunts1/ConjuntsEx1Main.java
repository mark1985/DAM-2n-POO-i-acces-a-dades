package conjunts1;

import java.util.HashSet;
import java.util.Set;

public class ConjuntsEx1Main {

	public static void main(String[] args) {
		Set<String> conjunt = new HashSet<String>();
		String str = "Aix� �s una l�nia de text de prova, prova �s aix�, una l�nia i nom�s una l�nia.";
		str = str.replace(",", "");
		str = str.replace(".", "");
		str = str.toUpperCase();
		String[] strArray = str.split(" ");
		String cadena = new String();
		int ocurrencias = 0;
			
		for(int i=0; i<strArray.length; i++){
			conjunt.add(strArray[i]);
		}
				
		for (String palabra : conjunt){
			ocurrencias = 0;
			for(int i=0; i<strArray.length; i++){

				if(palabra.equals(strArray[i])){
					ocurrencias++;
				}
			}
			cadena+=palabra+" ("+ocurrencias+")\n";
		}
		
		System.out.println(cadena);
	}

}
