package exercicis2Java;

import java.util.Scanner;

public class Exercici2Main {

	public static void main(String[] args) {
		
		PerDos x2 = new PerDos();
		NegPerDos d2 = new NegPerDos();
		
		System.out.println("Introdueix un numero (0 per inici per defecte)");
			Scanner scanner = new Scanner(System.in);
			double a = scanner.nextInt();
			System.out.println("Introdueix una serie (positiu per multiplicar i negatiu per dividir");
			double b = scanner.nextInt();
		
		if (a==0){
			a=x2.inicialitza();
		}
			
		if(b>0){
			x2.creaLlavor(a);
				
				for(int i=0; i<20; i++){
					a= x2.serialitza(a, b);
					System.out.format("%.2f, ",a);
				}
		}
		else{
			d2.creaLlavor(a);
			
				for(int i=0; i<20; i++){
					a= d2.serialitza(a, b*-1);
					System.out.format("%.2f, ",a);
				}
		}
		
	}

}
