package exercicis2Java;

public class PerDos implements Serie {
	private double a;

	@Override
	public double creaLlavor(double a) {
		return a;
	}

	@Override
	public double inicialitza() {
		this.a = 1;
		return a;
	}

	@Override
	public double serialitza(double a, double serie) {
		
		return a*serie;
	}

}
