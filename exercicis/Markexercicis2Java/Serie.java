package exercicis2Java;

public interface Serie {
	public double creaLlavor (double a);
	public double inicialitza ();
	public double serialitza (double a, double serie);
}
