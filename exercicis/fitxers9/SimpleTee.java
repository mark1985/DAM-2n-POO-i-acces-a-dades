package fitxers9;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class SimpleTee {

	public static void main(String[] args) {
		
		char ch;
		int k;
		
		if(args.length==1){
			try (InputStreamReader reader = new InputStreamReader(System.in);
					FileWriter writer = new FileWriter(args[0])) {
				while ((k = reader.read()) != -1) {
					ch = (char) k;
					System.out.print(ch);
					writer.write(k);
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
		else{
			System.err.println("Error en el numero de parametres");
		}

	}

}
