package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exercici4 {

	public static void main(String[] args) {
		
		String sql = "select s.store_id, ad.address, st.first_name, st.last_name from store s "
				+ "left join staff st on (st.staff_id=s.manager_staff_id) "
				+ "left join address ad on (ad.address_id=s.address_id);";

		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); Statement st = con.createStatement()){
					System.out.println("Connexio OK");
					System.out.println();
					
				try(ResultSet rs = st.executeQuery(sql)){
					
					while(rs.next()){
						System.out.println(rs.getString("s.store_id")+" "+rs.getString("ad.address")
						+" "+rs.getString("st.first_name")+" "+rs.getString("st.last_name"));
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
		
	}

}
