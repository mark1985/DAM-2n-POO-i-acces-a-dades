package exercicisInserts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
<<<<<<< HEAD
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
=======
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
>>>>>>> markJdbc3
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Exercici1 {

	public static void main(String[] args) {
		menu();

	}
	
	public static void menu(){
		Scanner sc = new Scanner(System.in);
		Integer opcio=0;
		
		do{
			System.out.println("1.- Fent servir Statement");
			System.out.println("2.- Fent servir PreparedStatement");
			System.out.println("3.- Fent servir SQL");
			System.out.println("0.- Sortir");
			System.out.println("Tria una opcio");
			
			opcio = sc.nextInt();
			
			switch(opcio){
			case 1:	utilizaStatement();
				break;
			case 2:	utilitzaPreparedStatement();
				break;
			case 3:	utilitzaSQL();
				break;
			case 0: System.out.println("Programa finalitzat");
				break;
			default: System.out.println("Opcio invalida");
			}
			
		}while(opcio!=0);
		
		sc.close();
	}
	
	public static void utilizaStatement(){
		String s;
		int numFiles = 0;
		String [] vector;
		long inicio = System.currentTimeMillis();
		
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/onomastica?"+
				"user=root"); Statement st = con.createStatement()){
					System.out.println("Connexio OK");
					System.out.println();
					st.executeUpdate("truncate noms2012");
			try (BufferedReader lector = new BufferedReader(new FileReader("noms_nascuts_2012.csv"))) {
				while ((s = lector.readLine()) != null) {
					vector = s.split(";");
					vector[4] = vector[4].replace(",", ".");
					
					if(vector[1].length()>20){
						vector[1] = vector[1].substring(0, 19);
					}
					
					numFiles += st.executeUpdate("INSERT INTO "
	                    + "noms2012(posicio, nom, sexe, frequencia, percentatge) "
	                    + "VALUES("+Integer.parseInt(vector[0])+",'"+vector[1]+"','"+vector[2]+"',"+Integer.parseInt(vector[3])+","+Float.parseFloat(vector[4])+");");
				}
			} catch (IOException e) {
				System.err.println("Error de lectura: " + e.getMessage());
				e.printStackTrace();
			}
		
		long termina = System.currentTimeMillis();
		
			System.out.println("Inserci� creada! Files afectades: " + numFiles);
			System.out.println("Temps transcorregut: "+(termina-inicio)+" milisegundos");
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}
	
	public static void utilitzaPreparedStatement(){
		String s;
		int numFiles = 0;
		String [] vector;
		long inicio = System.currentTimeMillis();
		
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/onomastica?"+
				"user=root"); PreparedStatement st = con.prepareStatement(
						"INSERT INTO "
	                    + "noms2012(posicio, nom, sexe, frequencia, percentatge) "
	                    + "VALUES(?,?,?,?,?);")){
					
					st.executeUpdate("truncate noms2012");
			try (BufferedReader lector = new BufferedReader(new FileReader("noms_nascuts_2012.csv"))) {
				while ((s = lector.readLine()) != null) {
					vector = s.split(";");
					vector[4] = vector[4].replace(",", ".");
					
					if(vector[1].length()>20){
						vector[1] = vector[1].substring(0, 19);
					}
					
					st.setInt(1, Integer.parseInt(vector[0]));
					st.setString(2, vector[1]);
					st.setString(3, vector[2]);
					st.setInt(4, Integer.parseInt(vector[3]));
					st.setFloat(5, Float.parseFloat(vector[4]));
					
					numFiles+=st.executeUpdate();
				}
			} catch (IOException e) {
				System.err.println("Error de lectura: " + e.getMessage());
				e.printStackTrace();
			}
		
		long termina = System.currentTimeMillis();
		
			System.out.println("Inserci� creada! Files afectades: " + numFiles);
			System.out.println("Temps transcorregut: "+(termina-inicio)+" milisegundos");
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}
	
	public static void utilitzaSQL(){
		String s;
		int numFiles = 0;
		String [] vector;
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO noms2012 VALUES ");
		long inicio = System.currentTimeMillis();
		
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/onomastica?"+
				"user=root"); Statement st = con.createStatement()){
					
				st.executeUpdate("truncate noms2012");
			try (BufferedReader lector = new BufferedReader(new FileReader("noms_nascuts_2012.csv"))) {
				while ((s = lector.readLine()) != null) {
					vector = s.split(";");
					vector[4] = vector[4].replace(",", ".");
					
					if(vector[1].length()>20){
						vector[1] = vector[1].substring(0, 19);
					}
					
					sql.append("("+Integer.parseInt(vector[0])+",'"+vector[1]+"','"+vector[2]+"',"+Integer.parseInt(vector[3])+","+Float.parseFloat(vector[4])+"),");
				}
				
				sql.setCharAt(sql.length()-1, ';');
				numFiles=st.executeUpdate(sql.toString());
				
			} catch (IOException e) {
				System.err.println("Error de lectura: " + e.getMessage());
				e.printStackTrace();
			}
		
			long termina = System.currentTimeMillis();
		
			System.out.println("Inserci� creada! Files afectades: " + numFiles);
			System.out.println("Temps transcorregut: "+(termina-inicio)+" milisegundos");
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}
	
	

}
