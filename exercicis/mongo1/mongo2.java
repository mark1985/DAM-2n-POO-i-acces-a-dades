package mongo1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;

public class mongo2 {

	public static void main(String[] args) {
		
		Integer[]rentalId ={1185,1476,1725};
		String[]rentalDate ={"2005-06-15 00:54:12.0","2005-06-15 21:08:46.0","2005-06-16 15:18:57.0"};
		String[]returnDate ={"2005-06-23 02:42:12.0","2005-06-25 02:26:46.0","2005-06-17 21:05:57.0"};
		Integer[]staffId={2,1,1};
		Integer[]filmId={611,308,159};
		String[]filmTitle={"MUSKETEERS WAIT","FERRIS MOTHER","CLOSER BANG"};
		
		Integer[]paymentId={3,5,6};
		Double[]amount={6.00,10.00,5.00};
		String[]paymentDate={"2005-06-15 00:54:12.0","2005-06-15 21:08:46.0","2005-06-16 15:18:57.0"};
		
		List<Document> payments = new ArrayList<Document>();
		for(int i=0; i<paymentId.length; i++){
			payments.add(new Document()
					.append("Payment ID", paymentId[i])
					.append("Amount", amount[i])
					.append("Payment Date", paymentDate[i])
			);
		}
				
		List<Document> rentals = new ArrayList<Document>();
		for(int i=0; i<rentalId.length; i++){
			rentals.add(new Document()
					.append("Rental ID", rentalId[i])
					.append("Rental Date", rentalDate[i])
					.append("Return Date", returnDate[i])
					.append("Staff ID", staffId[i])
					.append("Film ID", filmId[i])
					.append("Film Title", filmTitle[i])
					.append("Payments", Arrays.asList(payments.get(i)))
			);
		}
		
		Document main = new Document()
                .append("_id", 1)
                .append("First Name", "MARY")
                .append("Last Name", "SMITH")
                .append("Address", "1913 Hanoi Way")
                .append("District", "Nagasaki")
                .append("City", "Sasebo")
                .append("Country", "Japan")
                .append("Replacement Cost", 20.99)
                .append("Phone", "28303384290")
                .append("Rentals", rentals);
		
		JsonWriterSettings settings = new JsonWriterSettings(true);
        System.out.println(main.toJson(settings));
		
		
	}

}
