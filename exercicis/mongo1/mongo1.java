package mongo1;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.bson.json.JsonWriterSettings;

public class mongo1 {

	public static void main(String[] args) {
		
		List<Integer> actorId = new ArrayList<Integer>();
		actorId.add(5); actorId.add(27); actorId.add(37); 
		actorId.add(43); actorId.add(84); actorId.add(104);
		
		List<String> lastName = new ArrayList<String>();
		lastName.add("LOLLOBRIGIDA"); lastName.add("MCQUEEN"); lastName.add("BOLGER");
		lastName.add("JOVOVICH"); lastName.add("PITT"); lastName.add("CRONYN");
		
		List<String> firstName = new ArrayList<String>();
		firstName.add("JOHNNY"); firstName.add("JULIA"); firstName.add("VAL"); 
		firstName.add("KIRK"); firstName.add("JAMES"); firstName.add("PENELOPE");
		
		List<Document> arrayDoc = new ArrayList<Document>();
		Document document2;
		for(int i=0; i<actorId.size(); i++){
			document2 = new Document().append("actorId", actorId.get(i))
					.append("lastName", lastName.get(i))
					.append("firstName", firstName.get(i));
			arrayDoc.add(document2);
		}
				
		Document document = new Document()
                .append("_id", 19)
                .append("Title", "AMADEUS HOLY")
                .append("Description", "A Emotional Display of a Pioneer And a Technical Writer who must Battle a Man in A Baloon")
                .append("Length", 113)
                .append("Rating", "PG")
                .append("Special Features", "Commentaries,Deleted Scenes,Behind the Scenes")
                .append("Rental Duration", 6)
                .append("Replacement Cost", 20.99)
                .append("Category", "Action")
                .append("Actors", arrayDoc);

        JsonWriterSettings settings = new JsonWriterSettings(true);
        System.out.println(document.toJson(settings));

	}

}
