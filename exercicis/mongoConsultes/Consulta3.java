package mongoConsultes;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

public class Consulta3 {
		
	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("zips");
        MongoCollection<Document> collection = db.getCollection("zips");
                
        Bson sort = Sorts.orderBy(Sorts.descending("loc.1"));
        
        long i = collection.count();      
        collection.find().sort(sort).skip((int)i/2).limit(1).forEach((Document doc) -> System.out.println(doc.toJson()));

        client.close();

	}

}
