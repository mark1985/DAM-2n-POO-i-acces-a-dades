package mongoConsultes;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.bson.conversions.Bson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consultes7 {
	
	public static Double latitud = 0.00, longitud=0.00;
	public static int numSprings = 0, total = 0;
	public static Document ultReg = null;
	
	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("zips");
		MongoCollection<Document> collection = db.getCollection("zips");
		
		Bson filter = Filters.eq("city", "SPRINGFIELD");
		Bson sort = Sorts.ascending("State", "_id");
		
		collection.find(filter).sort(sort).forEach((Document doc) -> {
			
			if(ultReg == null ||  !ultReg.getString("State").equals(doc.getString("State"))){
				if (ultReg!=null) {
					System.out.println("Localitat: "+ultReg.getString("city")+" - GeoLoc: ("+longitud/numSprings+", "+latitud/numSprings+")");
				}
				System.out.println("Estat de "+doc.getString("State"));
				latitud = 0.00;
				longitud = 0.00;
				numSprings=0;
			}
			
			@SuppressWarnings("unchecked")
			List<Double>geoLoc = (List<Double>) doc.get("loc");
			latitud+=geoLoc.get(0);
			longitud+=geoLoc.get(1);
			numSprings++;
			ultReg = doc;
			total++;
			
		});
		System.out.println("Localitat: "+ultReg.getString("city")+" - GeoLoc: ("+longitud/numSprings+", "+latitud/numSprings+")");
		System.out.println("Quantitat de Springfields als EEUU: "+total);
		client.close();
	}
}
