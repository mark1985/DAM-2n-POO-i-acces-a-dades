package mongoConsultes;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consulta4 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("zips");
        MongoCollection<Document> collection = db.getCollection("zips");
        
        Bson filter = Filters.lt("pop",50);
        Bson sort = Sorts.orderBy(Sorts.descending("pop"));
        
        collection.find(filter).sort(sort).forEach((Document doc) -> System.out.println(doc.getString("city")+" "+doc.getInteger("pop")));

        client.close();

	}

}
