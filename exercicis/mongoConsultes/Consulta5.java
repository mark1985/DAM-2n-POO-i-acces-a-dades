package mongoConsultes;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consulta5 {

	public static void main(String[] args) {
			
		MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("zips");
        MongoCollection<Document> collection = db.getCollection("zips");
        
        Bson sortDes = Sorts.orderBy(Sorts.descending("_id"));
        Bson sortAsc = Sorts.orderBy(Sorts.ascending("_id"));
        
        collection.distinct("State", String.class).forEach((String st) -> {
        	Bson filter = Filters.eq("State", st);
        	System.out.println(st);
        	
        	collection.find(filter).sort(sortAsc).limit(1).forEach((Document doc) -> 
        		System.out.println(doc.getString("_id")+" "+doc.getString("city")));
        	collection.find(filter).sort(sortDes).limit(1).forEach((Document doc) -> 
        		System.out.println(doc.getString("_id")+" "+doc.getString("city")));
        });

        client.close();        
	}

}



