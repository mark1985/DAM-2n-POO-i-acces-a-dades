package mongoConsultes;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Consulta6 {
	
	public static int suma=0;
	
	public static void main(String[] args) {
		MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("zips");
        MongoCollection<Document> collection = db.getCollection("zips");
        
        Bson sortDes = Sorts.orderBy(Sorts.descending("_id"));
        Bson sortAsc = Sorts.orderBy(Sorts.ascending("_id"));
        Bson filter = Filters.eq("city", "KANSAS CITY");
        
        System.out.println("Kansas City");
        collection.find(filter).forEach((Document doc) -> {
        	
        	System.out.println(doc.getString("_id")+" num.Habitants: "+doc.getInteger("pop"));
        	suma+=doc.getInteger("pop");
        });
        
        System.out.println("Poblaci� total: "+suma);

        client.close(); 

	}

}
