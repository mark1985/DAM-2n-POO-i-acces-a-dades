package mongoConsultes;

import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class Consulta1 {

	public static void main(String[] args) {
		String pal = new String();
		
		Scanner sc = new Scanner(System.in);
		
		MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("zips");
        MongoCollection<Document> collection = db.getCollection("zips");
        
        System.out.println("Escriu el nom de la ciutat que vols");
        pal=sc.nextLine();
        
        Bson filter = Filters.eq("city",pal);
        
        collection.find(filter).forEach((Document doc) -> System.out.println(doc.toJson()));
        
        		
        sc.close();
        client.close();
	}

}
