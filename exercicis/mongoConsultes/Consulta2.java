package mongoConsultes;

import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

public class Consulta2 {

	public static void main(String[] args) {
		
		MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("zips");
        MongoCollection<Document> collection = db.getCollection("zips");
                
        Bson sort = Sorts.orderBy(Sorts.descending("pop"));
                
        collection.find().sort(sort).limit(10).forEach((Document doc) -> System.out.println(doc.toJson()));

        client.close();

	}

}
