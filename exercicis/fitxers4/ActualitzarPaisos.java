package fitxers4;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class ActualitzarPaisos {

	public static void main(String[] args) {
		String nom, codiISO, capital;
        int id, poblacio;
        long pos=0;
        Scanner scanner = new Scanner(System.in);
        String dada;
        StringBuilder dato;

        System.out.println("Introdueix numero de registre: ");
        id=scanner.nextInt();
        scanner.nextLine();
        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
            pos=(id-1)*174;

            if (pos<0 || pos>=fitxer.length())
                throw new IOException("N�mero de registre inv�lid.");

            fitxer.seek(pos);
            fitxer.readInt();
            nom = readChars(fitxer, 40);
            codiISO = readChars(fitxer, 3);
            capital = readChars(fitxer, 40);
            poblacio = fitxer.readInt();
            
            System.out.println("Pa�s: "+nom+", codi: "+codiISO+", capital: "+capital +", poblaci� actual: "+poblacio);
            System.out.println("Introdueix la dada que vols canviar: ");
            dada = scanner.nextLine();
            dada.toLowerCase();
            
            if(dada.equals("nom")){
            	System.out.println("Escriu el nou nom del pais: ");
            	dada = scanner.nextLine();
            	dato = new StringBuilder(dada);
            	dato.setLength(40);
            	pos = fitxer.getFilePointer()-170;
            	fitxer.seek(pos);
            	fitxer.writeChars(dato.toString());
            }
            else if(dada.equals("codi")){
            	System.out.println("Escriu el nou codi del pais: ");
            	dada = scanner.nextLine();
            	dato = new StringBuilder(dada);
            	dato.setLength(6);
            	pos = fitxer.getFilePointer()-90;
            	fitxer.seek(pos);
            	fitxer.writeChars(dato.toString());
            }
            else if(dada.equals("capital")){
            	System.out.println("Escriu la nova capital del pais: ");
            	dada = scanner.nextLine();
            	dato = new StringBuilder(dada);
            	dato.setLength(40);
            	pos = fitxer.getFilePointer()-84;
            	fitxer.seek(pos);
            	fitxer.writeChars(dato.toString());
            }
            else if(dada.equals("poblacio")){
            	System.out.println("Escriu la nova poblacio del pais: ");
            	poblacio = scanner.nextInt();
            	
            	if (poblacio >= 0) {
	                pos = fitxer.getFilePointer()-4;
	                fitxer.seek(pos);
	                fitxer.writeInt(poblacio);
	            } else {
	                System.err.println("La poblaci� ha de ser positiva.");
	            }
            }
            
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        scanner.close();
         
	}
	
	 private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
	        StringBuilder builder = new StringBuilder();
	        char ch = ' ';
	        for (int i=0; i<nChars; i++) {
	            ch=fitxer.readChar();
	            if (ch != '\0')
	            	builder.append(ch);
	        }
	        return builder.toString();
    }

}

