package exercici5Llistes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Agenda{
	private List <Contacte> agenda;
	
	public Agenda(){
		agenda = new ArrayList<Contacte>();
	}
	
	public void afegirContacte(Contacte persona){
		this.agenda.add(persona);
		
		this.agenda.sort(null);
	}
	
	public void esborrarContacte(String nom){
		Iterator<Contacte> it;
		it = this.agenda.iterator();
		Contacte persona;
		
		while (it.hasNext()){
			persona = it.next();
			if(persona.getNombre().equals(nom)){
				it.remove() ;
			}
		}

	}
	
	public Contacte cercarElement(String dato, String valor){
		Contacte persona = new Contacte();
		Contacte result = new Contacte();
		
		Iterator<Contacte> it;
		it = this.agenda.iterator();
		
		while (it.hasNext()){
			persona = it.next();

			switch (dato){
			case "nom": 	if(persona.getNombre().equals(valor)){
								result=persona;
							}
				break;
			case "cognom": 	if(persona.getApellido().equals(valor)){
								result=persona;
							}
				break;
			case "direccio": 	if(persona.getDireccion().equals(valor)){
									result=persona;
								}
				break;
			case "telefon": 	if(String.valueOf(persona.getTelf()).equals(valor)){
									result=persona;
								}
				break;
				default: System.out.println("Dada incorrecta");
			}
		}
		
		return result;
	}
	
	public void modificarInfo(Contacte persona, String dato, String valor){

		switch (dato){
		case "nom": persona.setNombre(valor);
			break;
		case "cognom": persona.setApellido(valor);
			break;
		case "direccio": persona.setDireccion(valor);
			break;
		case "telefon": persona.setTelf(Integer.parseInt(valor));
			break;
		}
	}
	
	public void mostrarAgenda(){
		
		for(Contacte p : this.agenda){
			System.out.println(p.getNombre()+" "+p.getApellido()+" "+p.getTelf()+" "+p.getDireccion());
		}
	}
	
}
