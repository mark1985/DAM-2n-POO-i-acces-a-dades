package exercici5Llistes;

public class Contacte implements Comparable <Contacte>{
	private String nombre;
	private String apellido;
	private int telf;
	private String direccion;
	
	public Contacte(){
		
	}
	public Contacte(String nom, String cognom, int telf, String direccion){
		this.nombre = new String();
		this.apellido = new String();
		this.nombre=nom;
		this.apellido=cognom;
		this.telf=telf;
		this.direccion=direccion;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTelf(int telf) {
		this.telf = telf;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public int getTelf() {
		return telf;
	}

	public String getDireccion() {
		return direccion;
	}

	@Override
	public int compareTo(Contacte arg0) {
		int comp;
		comp=this.getApellido().compareTo(arg0.getApellido());
		
		if (comp==0){
			comp=this.getNombre().compareTo(arg0.getNombre());
		}
		return comp;
	}

}
