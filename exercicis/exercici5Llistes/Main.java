package exercici5Llistes;

public class Main {

	public static void main(String[] args) {
		Agenda  agenda = new Agenda();
		Contacte p1 = new Contacte("Pedro", "Garcia", 931234567, "C/Sabadell, 1");
		Contacte p2 = new Contacte("Ramon", "Garcia", 933214567, "C/Sabadell, 2");
		Contacte p3 = new Contacte("Aina", "Garcia", 937575757, "C/Sabadell, 3");
		
		
		//COMPROBACIO DELS METODES D'AGENDA
		
		agenda.afegirContacte(p2);
		agenda.afegirContacte(p3);
		agenda.afegirContacte(p1);
		
		agenda.mostrarAgenda();
		
		agenda.esborrarContacte("Pedro");
		agenda.mostrarAgenda();
		
		Contacte cerca = new Contacte();
		cerca = (agenda.cercarElement("telefon", String.valueOf(937575757)));
		
		if(cerca.getNombre()!=null){
			System.out.println(cerca.getNombre()+" "+cerca.getApellido()+" "+cerca.getTelf()+" "+cerca.getDireccion());
		}
		else{
			System.out.println("No es troba el contacte");
		}
		
		agenda.modificarInfo(p3, "cognom", "Bio");
		agenda.mostrarAgenda();
		
	}

}
