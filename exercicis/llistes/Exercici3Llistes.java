package llistes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Exercici3Llistes {

	public static void main(String[] args) {
		String[] llenguatges = {"Java", "C", "C++", "Visual Basic", "PHP", "Python", "Perl", "C#", "JavaScript","Delphi"};
		int i=0;
		
		System.out.println("El mes popular es: "+llenguatges[0]);
		System.out.println("El sise mes popular es: "+llenguatges[5]);
		
		System.out.print("Els tres mes populars son:");
		
		for(int j=0;j<3;j++){
			System.out.print(" "+llenguatges[j]);
		}
		System.out.println();
		System.out.print("Els tres menys populars son:");
		for(int j=llenguatges.length-3;j<llenguatges.length;j++){
			System.out.print(" "+llenguatges[j]);
		}
		
		System.out.println();
		System.out.print("Tots menys el primer son:");
		for(int j=1;j<llenguatges.length;j++){
			System.out.print(" "+llenguatges[j]);
		}
		
		System.out.println();
		System.out.print("Llista numerada ");
		i=0;
		for (String a : llenguatges){
			i++;
			System.out.print(" "+i+".-"+a);
		}
		
		System.out.println();
		System.out.println("Nombre total de llenguatges = "+llenguatges.length);
	}

}
