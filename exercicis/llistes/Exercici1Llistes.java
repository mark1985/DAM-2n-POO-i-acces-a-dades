package llistes;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Exercici1Llistes {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner (System.in);
		
		String cadena = new String();
		List<String> llista = new ArrayList<String>();
		
		do{
			
			System.out.println("Escriu una cadena o res si vols acabar");
			cadena = scan.nextLine();
			llista.add(cadena);
			
		}while (!cadena.equals(""));
		
		do{
			
			System.out.println("Busca una cadena o res si vols acabar");
			cadena = scan.nextLine();
			
			if(llista.contains(cadena)){
				System.out.println(cadena+" es trobava a la llista");
			}
			else{
				System.out.println(cadena+" NO es trobava a la llista");
			}
			
		}while (!cadena.equals(""));
	}

}
