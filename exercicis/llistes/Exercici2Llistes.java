package llistes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercici2Llistes {

	public static void main(String[] args) {
		String[]ordenats = {"23","43","35","1","2"};
		String[]ordenats2 = {"1","2","3","4"};
		
		if(isOrdenat(ordenats)){
			System.out.println("La llista esta ordenada");
		}
		else{
			System.out.println("La llista NO esta ordenada");
		}
		
		if(isOrdenat(ordenats2)){
			System.out.println("La llista esta ordenada");
		}
		else{
			System.out.println("La llista NO esta ordenada");
		}
	}
	
	public static boolean isOrdenat (String[]str){
		boolean control = true;
		boolean result = false;
		int i=1;

		while (control == true && i < str.length){
			if(Integer.parseInt(str[i])>Integer.parseInt(str[i-1])){
				result=true;
			}
			else{
				result=false;
				control=false;
			}
			i++;
		}
		
		return result;
	}

}
