package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exercici3 {

	public static void main(String[] args) {
		String sql = "select film_id, store_id from inventory where inventory_id in "
				+ "(select inventory_id from rental where return_date is null and customer_id = "
				+ "(select customer_id from customer where (first_name)='ALLISON' and upper(last_name)='STANLEY'));";

		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); Statement st = con.createStatement()){
					System.out.println("Connexio OK");
					System.out.println();
					
				try(ResultSet rs = st.executeQuery(sql)){
					
					while(rs.next()){
						System.out.println(rs.getString("film_id")+" "+rs.getString("store_id"));
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}

}
