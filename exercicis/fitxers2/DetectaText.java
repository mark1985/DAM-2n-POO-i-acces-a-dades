package fitxers2;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DetectaText {

	public static void main(String[] args) {
		
		char letra;
		
		try(DataInputStream lector = new DataInputStream(new FileInputStream(args[0]))){
			
			while(true){
				letra=(char) lector.readByte();
					
					if(Character.isLetter(letra)==true){
						System.out.print(letra);
					}
			}
			
			
		} catch (EOFException e) {
			System.err.println("final");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
