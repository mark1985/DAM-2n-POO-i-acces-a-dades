package fitxers2;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class GeneraSecret {

	public static void main(String[] args) {
		Random rand = new Random();
		int codi;
		int suma;
		int index;
		char abc[]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
		String secret = new String();
		
		suma = rand.nextInt(3)+1;
		
		try(DataOutputStream writer = new DataOutputStream(new FileOutputStream("GeneraSecret.bin"))){
		
			for (int i=0;i<1000;i++){
				
				for(int j=0;j<3;j++){
					index=rand.nextInt(abc.length);
					secret+=abc[index];
				}
				
				codi=rand.nextInt(3)+1;
				codi = suma + codi;
					
				System.out.println(codi+" "+secret);
				writer.writeInt(codi);
				writer.writeChars(secret);
				secret="";
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
