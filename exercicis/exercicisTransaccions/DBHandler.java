package exercicisTransaccions;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

public class DBHandler {
	
	public boolean comprovaDept(String dept){
		boolean control = false;
		
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees?user=root");
				PreparedStatement st = con.prepareStatement("Select count(dept_name) from departments where lower(dept_name) = lower(?)")){
				
				if(!dept.equals(""))
					st.setString(1, dept);
				
				try(ResultSet rs = st.executeQuery()){
					
					while(rs.next()){
						control = rs.getInt(1)>0 ? true : false;
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
		}
		
		return control;
	}
	
	public int getEmpNum(){
		int id = 0;
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees?user=root");
				Statement st = con.createStatement()){

				try(ResultSet rs = st.executeQuery("Select max(emp_no)+1 from employees")){
					
					while(rs.next()){
						id = rs.getInt(1);
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
		}
		
		return id;
	}
	
	public String getDeptNum(Empleat emp){
		String id ="";
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees?user=root");
				Statement st = con.createStatement()){

				try(ResultSet rs = st.executeQuery("Select dept_no from departments where dept_name = '"+emp.getNomDept()+"'")){
					
					while(rs.next()){
						id = rs.getString(1);
					}
				}
				
		}catch(SQLException e){
			System.err.println(e.getMessage());
		}
		
		return id;
	}
	
	public void insertEmpleat(Empleat emp){
		String empleat = "INSERT INTO employees(emp_no, birth_date ,first_name, last_name, gender, hire_date) VALUES (?, ?, ?, ?, ?, ?)";
		
		String dept = "INSERT INTO dept_emp(emp_no, dept_no, from_date, to_date) VALUES (?, ?, ?, ?)";
		
		String salary = "INSERT INTO salaries(emp_no, salary, from_date, to_date) VALUES (?, ?, ?, ?)";
		
		String title = "INSERT INTO titles(emp_no, title, from_date, to_date) VALUES (?, ?, ?, ?)";
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees?user=root")){
	
			try (PreparedStatement insertEmp = con.prepareStatement(empleat);
                    PreparedStatement insertDept = con.prepareStatement(dept);
                    PreparedStatement insertSalary = con.prepareStatement(salary);
                    PreparedStatement insertTitle = con.prepareStatement(title);){
					
				con.setAutoCommit(false);
				
					insertEmp.setInt(1, getEmpNum());
					insertEmp.setDate(2, Date.valueOf("1990-01-01"));
					insertEmp.setString(3, emp.getNom());
					insertEmp.setString(4, emp.getCognom());
					insertEmp.setString(5, emp.getGenere());
					insertEmp.setDate(6, Date.valueOf(LocalDate.now().toString()));
					
					insertDept.setInt(1, getEmpNum());
					insertDept.setString(2, getDeptNum(emp));
					insertDept.setDate(3, Date.valueOf(LocalDate.now().toString()));
					insertDept.setDate(4, Date.valueOf("9999-01-01"));
					
					insertSalary.setInt(1, getEmpNum());
					insertSalary.setDouble(2, emp.getSalariInicial());
					insertSalary.setDate(3, Date.valueOf(LocalDate.now().toString()));
					insertSalary.setDate(4, Date.valueOf("9999-01-01"));
					
					insertTitle.setInt(1, getEmpNum());
					insertTitle.setString(2, emp.getTitolInicial());
					insertTitle.setDate(3, Date.valueOf(LocalDate.now().toString()));
					insertTitle.setDate(4, Date.valueOf("9999-01-01"));
					
					insertEmp.executeUpdate();
					insertDept.executeUpdate();
					insertSalary.executeUpdate();
					insertTitle.executeUpdate();
					
				con.commit();
                System.out.println("Transaccio realitzada!");
										
			}catch(SQLException e){
				System.err.print("Error a la transaccio, desfem els canvis");
                con.rollback();
                e.printStackTrace();
            } finally {
                con.setAutoCommit(true);
            }
		}catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
}
