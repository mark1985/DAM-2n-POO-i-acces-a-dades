package exercicisTransaccions;

import java.util.Scanner;

public class Empleat {
	private final String nom;
	private final String cognom;
	private final String genere;
	private final String nomDept;
	private final String titolInicial;
	private final Double salariInicial;
	
	private Empleat(String nom, String cognom, String genere, String nomDept, String titol, Double salari){
		this.nom = nom;
		this.cognom = cognom;
		this.genere = genere;
		this.nomDept = nomDept;
		this.titolInicial = titol;
		this.salariInicial = salari;
	}
	
	public static Empleat generaEmpleat(){
		DBHandler  db = new DBHandler();
		Scanner sc = new Scanner(System.in);
		String nom, cognom, genere, dept, titol;
		Double salari;
		
		System.out.println("Escriu el nom del empleat: ");
		nom = sc.nextLine();
		System.out.println("Escriu el cognom: ");
		cognom = sc.nextLine();
		System.out.println("Escriu el genere: ");
		genere = sc.nextLine();
		
		do{
			System.out.println("Escriu un nom valid pel departament: ");
			dept = sc.nextLine();
		}while(db.comprovaDept(dept)==false);
		
		System.out.println("Escriu la titulacio: ");
		titol = sc.nextLine();
		
		do{
			System.out.println("Escriu un salari (obviament positiu): ");
			salari = sc.nextDouble();
		}while(salari<1);
		
		sc.close();
		return new Empleat(nom, cognom, genere, dept, titol, salari);
	}

	public String getNom() {
		return nom;
	}

	public String getCognom() {
		return cognom;
	}

	public String getGenere() {
		return genere;
	}

	public String getNomDept() {
		return nomDept;
	}

	public String getTitolInicial() {
		return titolInicial;
	}

	public Double getSalariInicial() {
		return salariInicial;
	}
	
	
}
