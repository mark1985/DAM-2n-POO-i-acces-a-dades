package exercicis1Java;

import java.util.Comparator;

public class ComparadorPes implements Comparator<Persona>{
	
	@Override
	public int compare(Persona p1, Persona p2) {
		
		
		if (p1.getPes()>p2.getPes()){
			return 1;
		}
		if (p1.getPes()<p2.getPes()){
			return -1;
		}
		else {
			return 0;
		}
	}

}
