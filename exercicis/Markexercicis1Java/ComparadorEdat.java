package exercicis1Java;

import java.util.Comparator;

public class ComparadorEdat implements Comparator<Persona> {

	@Override
	public int compare(Persona p1, Persona p2) {
		
		
		if (p1.getEdat()>p2.getEdat()){
			return 1;
		}
		if (p1.getEdat()<p2.getEdat()){
			return -1;
		}
		else {
			return 0;
		}
	}

}
