package exercicis1Java;

public class PersonaMain {

	public static void main(String[] args) {
		Persona p1 = new Persona();
		Persona p2 = new Persona();
		
		ComparadorEdat CE = new ComparadorEdat();
		ComparadorPes PE = new ComparadorPes();
		
		System.out.println(""+" "+ p1.getAltura() +" Edat: "+ p1.getEdat() +" "+ p1.getPes());
		System.out.println(""+" "+ p2.getAltura() +" Edat: "+ p2.getEdat() +" "+ p2.getPes());
		System.out.println("Comparador Edat: "+ CE.compare(p1, p2));
		System.out.println("Comparador Pes: "+ PE.compare(p1, p2));
	}

}
