package exercicis1Java;
import java.util.Random;
import java.util.Comparator;

public class Persona implements Comparable<Persona>{

	private double pes;
	private int edat;
	private int altura;
	
	private Random rand = new Random();
	
	public Persona(){
		this.pes = rand.nextDouble()+50 ;
		this.edat = rand.nextInt(100);
		this.altura = rand.nextInt(200)+20;
	}
	
	@Override
	public int compareTo(Persona p) {
		return getAltura() - p.getAltura();
	}
	
	public int getAltura() {
		return altura;
	}

	public double getPes() {
		return pes;
	}

	public int getEdat() {
		return edat;
	}
	
	public static final Comparator<Persona> COMPARADOR_EDAT = new Comparator<Persona>(){
		@Override
		public int compare(Persona p1, Persona p2) {
			if (p1.getEdat()>p2.getEdat()){
				return 1;
			}
			if (p1.getEdat()<p2.getEdat()){
				return -1;
			}
			else {
				return 0;
			}
		}
		
	};
	
	public static final Comparator<Persona> COMPARADOR_PES = new Comparator<Persona>(){
		@Override
		public int compare(Persona p1, Persona p2) {
			if (p1.getPes()>p2.getPes()){
				return 1;
			}
			if (p1.getPes()<p2.getPes()){
				return -1;
			}
			else {
				return 0;
			}
		}
		
	};

}
