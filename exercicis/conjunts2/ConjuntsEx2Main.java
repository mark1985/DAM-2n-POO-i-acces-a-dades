package conjunts2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ConjuntsEx2Main {

	public static void main(String[] args) {
		Set<Integer> primerConjunt = new HashSet<Integer>();
		Set<Integer> segonConjunt = new HashSet<Integer>();
		Set<Integer> noDivisors = new HashSet<Integer>();
		Set<Integer> copiaAux = new HashSet<Integer>();
		Integer num1 = 0;
		Integer num2 = 0;
		
		
		List<Integer> llista = new ArrayList<Integer>();
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Escriu dos numeros enters");
		num1 = scan.nextInt();
		num2 = scan.nextInt();
		
		for(int i=2; i<1001; i++){
			
			if(num1%i==0){
				primerConjunt.add(i);
			}
			if(num2%i==0){
				segonConjunt.add(i);
			}
			else if(num1%i!=0 && num2%i!=0 && i<=100){
				noDivisors.add(i);
			}
		}
		
		copiaAux.addAll(primerConjunt);
		copiaAux.retainAll(segonConjunt);
		llista.addAll(copiaAux);
		llista.sort(null);
		
		System.out.println("Nombres del 2 al 1000 divisors dels dos nombres "+llista.toString());
		
		copiaAux.addAll(primerConjunt);
		copiaAux.addAll(segonConjunt);
		llista.clear();
		llista.addAll(copiaAux);
		llista.sort(null);
		
		System.out.println("Nombres del 2 al 1000 divisors de un dels dos nombres "+llista.toString());
		
		llista.clear();
		llista.addAll(noDivisors);
		llista.sort(null);
		
		System.out.println("Nombres del 2 al 100 no divisors de cap dels dos nombres "+llista.toString());
		scan.close();
	}

}
