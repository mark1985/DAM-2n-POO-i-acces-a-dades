package observerPattern;

import java.util.ArrayList;
import java.util.List;

public class GuardaEsdeveniments implements Listener {
	List<Integer>events = new ArrayList<Integer>();

	@Override
	public void notifyEvent(int event) {
		events.add(event);
	}
	
	@Override
	public String toString(){
		
		String cadena = "";
			
			for(int i=0; i<events.size(); i++){
				cadena+=events.get(i)+" ";
			}
		
		return cadena;
	}

}
