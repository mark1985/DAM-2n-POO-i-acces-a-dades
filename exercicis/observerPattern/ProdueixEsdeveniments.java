package observerPattern;

import java.util.ArrayList;
import java.util.List;

public class ProdueixEsdeveniments {
	private List<Listener>esdevenimentsProduits = new ArrayList<Listener>();
	
	public void addEventListener(Listener event){
		esdevenimentsProduits.add(event);
	}
	
	public void creaEsdeveniments (int event){
		
		for(int i=0; i<esdevenimentsProduits.size(); i++){
			esdevenimentsProduits.get(i).notifyEvent(event);
		}
	}

}
