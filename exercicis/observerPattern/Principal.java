package observerPattern;

public class Principal {

	public static void main(String[] args) {
		
		Listener guarda = new GuardaEsdeveniments();
		Listener mostra = new MostraEsdeveniments();
		ProdueixEsdeveniments produeix = new ProdueixEsdeveniments();
		
		produeix.addEventListener(guarda);
		produeix.addEventListener(mostra);
		
		for(int i=0; i<5; i++){
			produeix.creaEsdeveniments(i);
		}
		
		System.out.println(guarda.toString());
	}

}
