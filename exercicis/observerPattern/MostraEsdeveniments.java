package observerPattern;

public class MostraEsdeveniments implements Listener {

	@Override
	public void notifyEvent(int event) {
		System.out.println("Event ID: "+ event);
	}

}
