package mongo2;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Exercici1Insert {

	public static void main(String[] args) {
		
		String line;
		String[]vector;
		String[]location;
		
		MongoClient mongo = new MongoClient();
		MongoDatabase db = mongo.getDatabase("zips");
		MongoCollection<Document> collection = db.getCollection("zips");
		
		collection.drop();
		
		try (LineNumberReader lector = new LineNumberReader(new FileReader("zips.json"))) {
			while ((line = lector.readLine())!= null) {
				
				line=line.replaceAll(",", "");
				line=line.replaceAll(":", "");
				line=line.replaceAll("\\[", "");
				line=line.replaceAll("\\]", "");
				
				vector=line.split("\"");
				location=vector[10].split(" ");
				
				
				
				Document doc = new Document("_id",vector[3])
						.append("city", vector[7])
						.append("location", Arrays.asList(location[3],location[4]))
						.append("poblacion", vector[12]=vector[12].replaceAll(" ", ""))
						.append("CP", vector[15]);
				
				System.out.println(vector[3]+" "+vector[7]+" "+location[3]+" "+location[4]+" "+vector[12]+" "+vector[15]);
				
				collection.insertOne(doc);
			}
		} catch (IOException e) {
			System.err.println("Error de lectura: " + e.getMessage());
			e.printStackTrace();
		}
		
		mongo.close();
		
	}

}
