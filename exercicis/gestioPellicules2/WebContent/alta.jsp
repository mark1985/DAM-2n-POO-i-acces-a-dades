<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Nova pel·lícula</title>
</head>
<jsp:useBean id="film" scope="request" class="beans.Film"/>
<jsp:setProperty name="film" property="*"/>
<%
if (request.getParameter("filmId")!=null) {%>
<jsp:forward page="/controlador?accio=inserir"/>
<%} %>
<body>
<%
if (request.getParameter("error")!=null) {%>
	<h2><%= request.getParameter("error").toString() %></h2>
<%} %>
  <h2>Entrada de dades de pel·lícula</h2>
  <form method="post">
    <p>Codi de pel·lícula: <input name="filmId" type="text" size="5"></p>
    <p>Nom: <input name="title" type="text" size="50"></p>
    <p>Descripció: <textarea name="description" rows="5" cols="50"></textarea></p>
    <p>Any: <input name="releaseYear" type="text" size="4"></p>
    <p>Idioma:  <select name="languageName">
				  <option value="English">English</option>
				  <option value="Italian">Italian</option>
				  <option value="Japanese">Japanese</option>
				  <option value="Mandarin">Mandarin</option>
				  <option value="French">French</option>
				  <option value="German">German</option>
				</select> </p>
    <p>Idioma original: <select name="originalLanguageName">
				  <option value="English">English</option>
				  <option value="Italian">Italian</option>
				  <option value="Japanese">Japanese</option>
				  <option value="Mandarin">Mandarin</option>
				  <option value="French">French</option>
				  <option value="German">German</option>
				</select> </p>
    <p>Durada: <input name="length" type="text" size="3"></p>
    <input type="submit" name="commit" value="Inserir pel·lícula">
    <input type="reset" name="cancel" value="Cancel·lar">
  </form>
</body>
</html>