package beans;

public class Film {
	private int filmId;
	private String title;
	private String description;
	private String releaseYear;
	private Integer languageId;
	private Integer originalLanguageId;
	private String languageName;
	private String originalLanguageName;
	private int length;
	public int getFilmId() {
		return filmId;
	}
	public Film() {
		;
	}
	public Film(int filmId, String title, String description,
			String releaseYear, Integer languageId, Integer originalLanguageId,
			int length, String language) {
		super();
		this.filmId = filmId;
		this.title = title;
		this.description = description;
		this.releaseYear = releaseYear;
		this.languageId = languageId;
		this.originalLanguageId = originalLanguageId;
		this.length = length;
		this.languageName = language;
	}
	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReleaseYear() {
		return releaseYear.substring(0, 4);
	}
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	public Integer getLanguageId(String name) {
		Integer languageId = 0;
		
		switch (name) {
		case "English": languageId = 1;
			break;
		case "Italian": languageId = 2;
			break;
		case "Japanese": languageId = 3;
			break;
		case "Mandarin": languageId = 4;
			break;
		case "French": languageId = 5;
			break;
		case "German": languageId = 6;
			break;
		}
		return languageId;
	}
	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}
	public Integer getOriginalLanguageId(String name) {
		Integer languageId = 0;
		
		switch (name) {
		case "English": languageId = 1;
			break;
		case "Italian": languageId = 2;
			break;
		case "Japanese": languageId = 3;
			break;
		case "Mandarin": languageId = 4;
			break;
		case "French": languageId = 5;
			break;
		case "German": languageId = 6;
			break;
		}
		return languageId;
	}
	public void setOriginalLanguageId(Integer originalLanguageId) {
		this.originalLanguageId = originalLanguageId;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getOriginalLanguageName() {
		return originalLanguageName;
	}
	public void setOriginalLanguageName(String originallanguageName) {
		this.originalLanguageName = originallanguageName;
	}
	@Override
	public String toString() {
		return "Film [filmId=" + filmId + ", title=" + title + ", description="
				+ description + ", releaseYear=" + releaseYear
				+ ", languageId=" + languageId + ", originalLanguageId="
				+ originalLanguageId + ", length=" + length + "]";
	}
	
}
