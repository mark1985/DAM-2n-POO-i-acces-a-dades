package exercicisDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exercici1 {

	public static void main(String[] args) {
		
		String sql = "select title, description from film where length > 149;";
		
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); Statement st = con.createStatement()){
					System.out.println("Connexio OK");
					System.out.println();
					
				try(ResultSet rs = st.executeQuery(sql)){
					
					while(rs.next()){
						System.out.println(rs.getString("title")+" "+rs.getString("description"));
					}
				}
				
		}catch(SQLException e){
			System.err.println("Error de connexio"+e.getMessage());
			
		}
	}
}
