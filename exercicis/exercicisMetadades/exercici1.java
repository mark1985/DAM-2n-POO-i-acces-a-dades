package exercicisMetadades;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class exercici1 {

	public static void main(String[] args) {
		
		DatabaseMetaData dbmd = null;
		
		String nomTaula = new String();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Escriu el nom de la taula que vols");
		nomTaula = sc.nextLine();
		
		try (Connection connection = DriverManager.getConnection
	            ("jdbc:mysql://localhost:3306/sakila?user=root");) {
	        
			dbmd = connection.getMetaData();
			try(ResultSet keys = dbmd.getPrimaryKeys(null, "sakila", nomTaula)){
				while(keys.next())
					System.out.println("Primaries "+keys.getString(4));
			}
			try(ResultSet keys = dbmd.getExportedKeys(null, "sakila", nomTaula)){
				while(keys.next())
					System.out.println("Exportades "+keys.getString(4));
			}
			try(ResultSet keys = dbmd.getImportedKeys(null, "sakila", nomTaula)){
				while(keys.next())
					System.out.println("Importades "+keys.getString(4));
			}
	        
	    } catch (SQLException e) {
	        System.err.println(e.getMessage());
	    }
		
		sc.close();

	}

}
