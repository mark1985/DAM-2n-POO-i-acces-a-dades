package exercicisMetadades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Scanner;

public class exercici2 {

	public static void main(String[] args) {
		ResultSetMetaData rsmd = null;
		String sql = new String();
		Scanner sc = new Scanner(System.in);
		int i = 0;
		
		System.out.println("Escriu la consulta que vols");
		sql = sc.nextLine();
		
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?"+
				"user=root"); PreparedStatement st = connection.prepareStatement(sql)) {
	        			
			try (ResultSet rs = st.executeQuery()) {
				rsmd = rs.getMetaData();
				int numCol = rsmd.getColumnCount();
				
				for(i=1; i<=numCol; i++){
					System.out.print(rsmd.getColumnName(i)+"\t");
				}
				System.out.println();
				
				while(rs.next()){
					for(i=1; i<=numCol; i++){
						System.out.print(rs.getString(i)+"\t\t");
							
						if(i==numCol){
							System.out.println();
						}
					}
				}
				
				
            }			
	        
	    } catch (SQLException e) {
	        System.err.println(e.getMessage());
	    }
		
		sc.close();

	}

}
