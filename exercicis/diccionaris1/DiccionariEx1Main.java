package diccionaris1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DiccionariEx1Main {

	public static void main(String[] args) {
		
		Map<String,Integer> llistaCompra = new HashMap<String,Integer>();
		
		llistaCompra.put("tomaquets", 6);
		llistaCompra.put("flams", 4);
		llistaCompra.put("pizzes", 2);
		llistaCompra.put("llaunes de tonyina", 2);
		llistaCompra.put("blat de moro", 5);
		llistaCompra.put("enciam", 1);
		
		System.out.println(llistaCompra.keySet());
		System.out.println(llistaCompra.get("tomaquets"));
		
		for(String ingredient : llistaCompra.keySet()){
			
			if(llistaCompra.get(ingredient)>=3){
				System.out.println(ingredient);
			}
		}
		
		Scanner scan = new Scanner(System.in);
		String cadena = new String();
		
		System.out.println("Escriu un ingredient");
		cadena = scan.nextLine();
		
		if(llistaCompra.containsKey(cadena)){
			System.out.println(llistaCompra.get(cadena));
		}
		else{
			System.err.println("Aquest ingredient no es a la llista");
		}
		
		for(String ingredient : llistaCompra.keySet()){
			
			System.out.println(ingredient+"("+llistaCompra.get(ingredient)+")");
		}
		
		int num = 0;
		
		for(String ingredient : llistaCompra.keySet()){
			
			num+=llistaCompra.get(ingredient);
		}
		System.out.println("Suma: "+num);
		
		System.out.println("Escriu un ingredient");
		cadena = scan.nextLine();
		
		if(llistaCompra.containsKey(cadena)){
			System.out.println("Escriu la nova quantitat");
			num = scan.nextInt();
			llistaCompra.put(cadena, num);
		}
		else{
			System.err.println("Aquest ingredient no es a la llista");
		}
		
		System.out.println("Nova quantitat de "+cadena+"("+llistaCompra.get(cadena)+")");
		
		scan.close();
	}

}
